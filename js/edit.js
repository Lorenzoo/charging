var Editor = {

  options: {
    language: undefined
  },

  init: function () {

  },

  show: function (id) {
    document.getElementById("editor-visible").checked = true;
  },

  hide: function () {
    document.getElementById("editor-visible").checked = false;
  },

  slideToEnd: function () {
    editorSwiper.slideTo(editorSwiper.slides.length - 1);
  }
}
