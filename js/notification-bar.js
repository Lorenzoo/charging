var NotificationBar = {

  update: function () {
    var notIndicator = document.getElementById("notification-indicator");
    var text = undefined;
    var animationTime = 500;

    if (map.getZoom() < StationLayer.options.minZoom && !route.allRoutes) {
      text = Locals.getText('notification-bar', 'pleaseZoomIn');
    }

    if (route.allRoutes) {
      var sequenceFound = false;
      Object.keys(route.allRoutes).forEach(function(key) {
        if (route.allRoutes[key].stationSequenceFound === true) {
          sequenceFound = true;
        }
      });
      if (! sequenceFound) {
        text = Locals.getText('notification-bar', 'noStationSequenceFound');
      }
    }

    if (! StationLayer._isOneSocketEnabled()) {
      text = Locals.getText('notification-bar', 'noSocketSelected');
    }

    if (text) {
      notIndicator.innerHTML = text;
      notIndicator.style.display = "flex";
      setTimeout(function() {
        document.getElementById("notification-indicator-checkbox").checked = true;
      }, 30);
    } else {
      setTimeout(function() {
        document.getElementById("notification-indicator-checkbox").checked = false;
      }, 30);
      // removes spacing from position=relative
      setTimeout(function() {
        notIndicator.style.display = "none";
      }, 600);
    }
  }

};
