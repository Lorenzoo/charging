var locate = {

  curPosition: undefined,
  marker: undefined,
  isCenteredOnMarker: false,

  init: function () {
    map.locate({
      enableHighAccuracy: true,
      watch: true,
      maximumAge: 1000
    });
    map.on('locationfound', locate.onLocationFound);
    map.on('locationerror', locate.onLocationError);
  },

  onLocationFound: function (e) {
    locate.curPosition = e;
    if (locate.marker) {
      locate.moveMarker();
    } else {
      locate.addMarker();
    }
    if (locate.isCenteredOnMarker) {
      locate.enableCenteringOnMarker();
    }
    if (navigation.active) {
      navigation.update();
    }
  },

  onLocationError: function (e) {
    console.log("error:");
    console.log(e.message);
  },

  addMarker: function () {
    var e = locate.curPosition;
    console.log(e);
    locate.marker = {};
    locate.marker.locationPulse = L.marker(
      e.latlng,
      { icon: L.divIcon({
        iconSize: [14,14],
        iconAnchor: [7,7],
        className: "",
        html: '<div id="curLocationPulse"></div>'
      }) }
    ).addTo(map);
    locate.marker.locationCenterBack = L.marker(
      e.latlng,
      { icon: L.divIcon({
        iconSize: [18,18],
        iconAnchor: [9,9],
        className: "",
        html: '<div id="curLocationBack"></div>'
      }) }
    ).addTo(map);
    locate.marker.locationCenter = L.marker(
      e.latlng,
      { icon: L.divIcon({
        iconSize: [14,14],
        iconAnchor: [7,7],
        className: "",
        html: '<div id="curLocation"></div>'
      }) }
    ).addTo(map);
    locate.marker.locationRadius = L.circle(e.latlng, {
      radius: e.accuracy / 2,
      stroke: false,
      color: "#009FE3",
      interactive: false
    }).addTo(map);
  },

  moveMarker: function () {
    if (!locate.marker) {
      return;
    }

    var e = locate.curPosition;
    Object.keys(locate.marker).forEach(function(key) {
      locate.marker[key].setLatLng([e.latlng.lat, e.latlng.lng]);
    });
    if (locate.isCenteredOnMarker) {
      map.flyTo([e.latlng.lat, e.latlng.lng], locate.getBestZoomLevel());
    }
  },

  centerOnMarker: function () {
    if (locate.isCenteredOnMarker) {
      locate.disableCenteringOnMarker();
    } else {
      locate.enableCenteringOnMarker();
    }
  },

  enableCenteringOnMarker: function () {
    locate.isCenteredOnMarker = true;
    var button = document.getElementById("locateMe");
    button.style.backgroundColor = "#009FE3";
    button.style.color = "white";
    locate.moveMarker();

    if (!locate.curPosition) {
      locate.init();
    }
  },

  disableCenteringOnMarker: function () {
    locate.isCenteredOnMarker = false;
    var button = document.getElementById("locateMe");
    button.style.backgroundColor = "white";
    button.style.color = "#5b5b5b";
  },

  getBestZoomLevel: function () {
    var target;
    if (navigation.active) {
      var coordinates  = route.allRoutes.curRoute.coordinates,
          instructions = route.allRoutes.curRoute.instructions;
      target = L.latLng(
        coordinates[instructions[navigation.nextInstruction].index].lat,
        coordinates[instructions[navigation.nextInstruction].index].lng
      );
    } else {
      var stations = stationResources[StationLayer.options.type].data;
      for (var i = 0; i < stations.length; i++) {
        stations[i].distanceToCurPosition = Math.ceil(locate.curPosition.latlng.distanceTo([stations[i].lat, stations[i].lon]));
      }
      stations.sort(compairDistance);
      target = L.latLng(stations[0].lat, stations[0].lon);
    }

    var latDiff = locate.curPosition.latlng.lat - target.lat,
        lngDiff = locate.curPosition.latlng.lng - target.lng;

    latDiff = latDiff * 1.2;
    lngDiff = lngDiff * 1.2;

    var bounds = [[locate.curPosition.latlng.lat + latDiff, locate.curPosition.latlng.lng + lngDiff],
                  [locate.curPosition.latlng.lat - latDiff, locate.curPosition.latlng.lng - lngDiff]];
    var zoomLvl = map.getBoundsZoom(bounds);

    return zoomLvl;

    function compairDistance (a, b) {
      if (a.distanceToCurPosition < b.distanceToCurPosition) {
        return -1;
      }
      if (a.distanceToCurPosition > b.distanceToCurPosition) {
        return 1;
      }
      // a must be equal to b
      return 0;
    }
  }
}
