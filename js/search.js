var search = {

  names: {},
  results: [],
  startPlaceholder: "",

  init: function() {
    document.getElementById("search-route-checkbox").checked = false;

    document.querySelector(".leaflet-routing-geocoder input").onfocus = function () {
      var element = document.querySelector(".leaflet-routing-container");
      element.className = element.className + ' leaflet-routing-container-focus';
    };
    document.querySelector(".leaflet-routing-geocoder input").onblur = function () {
      var element = document.querySelector(".leaflet-routing-container");
      element.className = element.className.replace(' leaflet-routing-container-focus', '');
    };

    document.getElementById("search-container").onclick = function() { menuBarSwiper.slideTo(1) };
  },

  moveToLocation: function(waypoint) {
    /*
    for (var i = 0; i < search.results.length; i++) {
      if (search.results[i].center === waypoint.latLng && search.results[i].name === waypoint.name) {
        break;
      }

      if (i === search.results.length - 1) {
        return;
      }
    }

    var chosen = search.results[i];
    if (chosen === undefined) {
      return;
    }
    */
    if (!waypoint.bbox) {
      return;
    }

    var bounds = L.latLngBounds(waypoint.bbox._southWest, waypoint.bbox._northEast);

    map.fitBounds(bounds);
    /*
    if (search.surrundingPolyLine) {
      search.surrundingPolyLine.removeFrom(map);
    }

    if (chosen.properties.polygonpoints !== undefined) {
      var list = [];
      for (var k = 0; k < chosen.properties.polygonpoints.length; k++) {
        list.push(new L.LatLng(chosen.properties.polygonpoints[k][1], chosen.properties.polygonpoints[k][0]));
      }
      search.surrundingPolyLine = new L.polyline(list, {
        color: 'red',
        weight: 3,
        opacity: 0.5,
        smoothFactor: 1
      });

      search.surrundingPolyLine.addTo(map);
    }
    */
  },

  formatData: function(json) {
    var i;
    search.results = [];

    // clear old search results
    search.names = {};

    for(i in json) {
      search.addToNames(json[i]);
    }

    var collidingKey;
    while ((collision = search.doDisplayNamesCollide()) !== false) {
      var collidingKey = collision.key;
      var classType = collision.classType;
      var length = search.names[classType][collidingKey].length;
      for (var i = 0; i < length; i++) {
        var tmpProblem = search.names[classType][collidingKey].pop();
        search.addToNames(tmpProblem);
      }
    }

    for (var classType in search.names) {
      if (!search.names.hasOwnProperty(classType) || classType === "length") {
        continue;
      }
      for (var key in search.names[classType]) {
        if (search.names[classType].hasOwnProperty(key)) {
          var searchResult = search.names[classType][key][0];
          if (searchResult === undefined) {
            continue;
          }
          var e = {
            name: key,
            category: searchResult.properties.category,
            html: search.generateIconHTML(searchResult.properties.category) + " <span class='search-result-text'> " + key + " </span>",
            bbox: searchResult.bbox
              ? L.latLngBounds(L.latLng(searchResult.bbox[1], searchResult.bbox[0]),L.latLng(searchResult.bbox[3], searchResult.bbox[2]))
              : L.latLngBounds(L.latLng(searchResult.center[1], searchResult.center[0]), L.latLng(searchResult.center[1], searchResult.center[0])),
            center: L.latLng(searchResult.center[1], searchResult.center[0]),
            relevance: searchResult.place_type.indexOf("place") != -1 ? searchResult.relevance + 0.5 : searchResult.relevance
          }
          search.results.push(e);
        }
      }
    }

    // sort to upvote places
    search.results.sort(compareRelevance);
    // remove all elements after the fifth
    search.results.splice(5);

    return search.results;

    function compareRelevance(a, b) {
      if (a.relevance > b.relevance) {
        return -1;
      }
      if (a.relevance < b.relevance) {
        return 1;
      }
      // a must be equal to b
      return 0;
    }
  },

  addToNames: function (object) {
    var displayName = search.generateDisplayName(object);

    if (displayName === false) {
      return false;
    }
    // TODO Unterscheidung noch nötig?
    var classType = object.place_type[0];

    if (search.names[classType] === undefined) {
      search.names[classType] = {};
    }

    if (search.names[classType][displayName] === undefined) {
      search.names[classType][displayName] = [];
    }

    search.names[classType][displayName].push(object);
  },

  generateDisplayName: function (searchResult) {
    var displayName = "";
    var levelOfDetail = 0;
    if (searchResult.levelOfDetail != undefined) {
      searchResult.levelOfDetail ++;
      levelOfDetail = searchResult.levelOfDetail;
    } else {
      searchResult.levelOfDetail = 0;
    }

    // level 0 - always add this parts to the display name
    // name
    displayName = searchResult.text;
    // house number
    if (searchResult.address) {
      displayName += searchResult.address;
    }

    // city name
    if (searchResult.context) {
      for (var i = 0; i < searchResult.context.length; i++) {
        if (searchResult.context[i].id.indexOf("place") != -1) {
          displayName += ", " + searchResult.context[i].text;
        }
      }
    }

    // level 3 - postcode
    if (searchResult.context && levelOfDetail > 1) {
      for (var i = 0; i < searchResult.context.length; i++) {
        if (searchResult.context[i].id.indexOf("postcode") != -1) {
          displayName += ", " + searchResult.context[i].postcode;
        }
      }
    }

    // level 1 - region
    if (searchResult.context && levelOfDetail > 0) {
      for (var i = 0; i < searchResult.context.length; i++) {
        if (searchResult.context[i].id.indexOf("region") != -1) {
          displayName += ", " + searchResult.context[i].region;
        }
      }
    }

    // level 2 - country
    if (searchResult.context && levelOfDetail > 1) {
      for (var i = 0; i < searchResult.context.length; i++) {
        if (searchResult.context[i].id.indexOf("country") != -1) {
          displayName += ", " + searchResult.context[i].country;
        }
      }
    }

    return displayName;
  },

  doDisplayNamesCollide: function () {
    for (var classType in search.names) {
      if (!search.names.hasOwnProperty(classType) || classType === "length") {
        continue;
      }
      for (var key in search.names[classType]) {
        if (!search.names[classType].hasOwnProperty(key) || key === "length") {
          continue;
        }
        if (search.names[classType][key].length > 1) {
          var name;
          for (var i = 0; i < search.names[classType][key].length; i++) {
            var e = search.names[classType][key][i];
            var oldLvl = e.levelOfDetail;
            e.levelOfDetail = 3; //max lvl of detail possible
            var tmpName = search.generateDisplayName(e);
            e.levelOfDetail = oldLvl;
            if (name === undefined || name === tmpName) {
              name = tmpName;
            } else {
              return {"key": key, "classType": classType};
            }
          }
        }
      }
    }

    return false;
  },

  generateIconHTML: function (category) {
    // TODO: add more icons
    var
      place = "<span class='fa fa-map-marker search-result-icon search-result-icon-place' aria-hidden='true'></span>",
      building = "<span class='fa fa-building search-result-icon' aria-hidden='true'></span>",
      airport = "<span class='fa fa-plane search-result-icon' aria-hidden='true'></span>",
      museum = "<span class='fa fa-university search-result-icon search-result-icon-museum' aria-hidden='true'></span>";
      fast_food = "<span class='fa fa-cutlery search-result-icon' aria-hidden='true'></span>";
      trainStation = "<span class='fa fa-subway search-result-icon' aria-hidden='true'></span>";

    if (category === undefined) {
      return place;
    }

    if (category.indexOf("train station") != -1 || category.indexOf("rail station") != -1) {
      return trainStation;
    }

    if (category.indexOf("building") != -1) {
      return building;
    }

    if (category.indexOf("airport") != -1) {
      return airport;
    }

    if (category.indexOf("museum") != -1) {
      return museum;
    }

    if (category.indexOf("fast food") != -1 || category.indexOf("restaurant") != -1) {
      return fast_food;
    }

    if (category != undefined) {
      //console.log("unknown search icon category: " + category);
    }

    return place;
  },

  expandSearchbar: function () {
    this.addMyLocationWaypoint();

    document.getElementById("search-route-checkbox").checked = true;
    var time = 300;
      // seting the search-container's z-index above stations-info's
      // (9900 or 9902 dependent on window width)
      // else .mapMenu elements would be above the fullscreen search bar
      document.getElementById("search-container").style["z-index"] = 9903;
      document.getElementById("menu-bar-container").style["z-index"] = 9900;
      document.getElementById("leaflet-routing-alternatives-container").style["z-index"] = 0;
      document.getElementById("route-controls").style.position = "absolute";
      document.querySelector(".leaflet-routing-container").style.overflow = "auto";
      // calculate the final hight
      var fullHeight = 0;
      // + inputs
      for (var i = 0; i < document.querySelectorAll(".leaflet-routing-geocoder").length; i++) {
        fullHeight += document.querySelectorAll(".leaflet-routing-geocoder")[i].clientHeight;
      }
      // + add waypoint button
      fullHeight += document.querySelector(".leaflet-routing-add-waypoint").clientHeight;
      // + distanceMeter height
      fullHeight += document.getElementById("distanceMeter").clientHeight;
      //$('.leaflet-routing-container').animate({ height: fullHeight }, time);
      $('#search-container').animate({ height: "100%", top: "0" }, time);
      search.setPlaceholder();
  },

  diminishSearchbar: function () {
    var time = 300;
    document.getElementById("search-route-checkbox").checked = false;
    document.getElementById("menu-bar-container").style["z-index"] = 9990;
    document.getElementById("route-controls").style.position = "relative";
    document.querySelector(".leaflet-routing-container").style.overflow = "hidden";
    if (navigation.active) {
      $('#search-container').animate({ height: "9em", top: "-10em" }, time);
    } else {
      $('#search-container').animate({ height: "12.5em", top: "-10em" }, time);
    }
    search.setPlaceholder();

    // smoothly make #menu-bar-button visible
    setTimeout(function() {
      document.getElementById("search-container").style["z-index"] = 9901;
      document.getElementById("menu-bar-button").style.opacity = 0;
      $('#menu-bar-button').animate({ opacity: "1"}, time);
    }, time);
  },

  cancleRoute: function () {
    var time = 300;
    // remove route line, routewaypoint and station markers
    leafletRoutingMachine._clearLines();
    // the timeout is necessary to avoid problems with the transition -
    // the splice method seems to first remove and then add new inputs
    // this way the new inputs would disrupt the beautiful transition
    // of the old inputs background-color
    //setTimeout(function() {
      leafletRoutingMachine._plan.spliceWaypoints(0 ,leafletRoutingMachine._plan._waypoints.length - 1, {latLng: undefined});
      search.setPlaceholder();
    //}, time);
    route.removeMarkers();
    navigation.hide();
    search.diminishSearchbar();
  },

  startRoute: function () {
    navigation.show();
    search.diminishSearchbar();
  },

  viewRoute: function () {
    navigation.hide();
    search.diminishSearchbar();
  },

  setPlaceholder: function () {
    var inputs = document.querySelectorAll(".leaflet-routing-geocoder input");
    if (document.getElementById("search-route-checkbox").checked) {
      // reset "end"
      inputs[inputs.length - 1].placeholder = search.startPlaceholder;
    } else {
      // set "search"
      search.startPlaceholder = inputs[inputs.length - 1].placeholder;
      inputs[inputs.length - 1].placeholder = Locals.getText('search-bar', 'search');
    }
  },

  addMyLocationWaypoint: function (force) {
    if (force || !leafletRoutingMachine.getWaypoints()[0].name) {
      if (locate.curPosition) {
        leafletRoutingMachine.spliceWaypoints(0, 1, {latLng: locate.curPosition.latlng, name: Locals.getText('search-bar', 'my-position'), options: {noReverseGeocode: true}});
      } else if (force) {
        window.alert("I don't know your position yet");
      }

    }
  },

  removeMyLocationWaypoint: function () {
    if (leafletRoutingMachine.getWaypoints()[0].name == Locals.getText('search-bar', 'my-position')) {
      leafletRoutingMachine.spliceWaypoints(0, 1, {latLng: undefined});
    }
  },

  changeMaxDistance: function (distance) {
    route.maxDistance = distance;
    route.reInit();
    urlHash && urlHash.onChange();
  },

  checkNewManualDistance: function () {
    distance = document.getElementById("distanceMeterManualInput").value.toLowerCase().trim();
    if (distance.indexOf("km") != -1) {
      distance = distance.replace("km", "");
    }

    if (distance == '' || isNaN(distance)) {
      search.showManualDistanceErrorNotification();
      return;
    }

    document.getElementById("distanceMeterManualExclamation").style.display = "none";
    search.changeMaxDistance(distance * 1000);
  },

  showManualDistanceErrorNotification: function () {
    document.getElementById("distanceMeterManualExclamation").style.display = "block";
  },

  setDistanceByHash: function (distance) {
    switch (distance) {
      case "150":
        document.getElementById("distanceMeterLow-radio").checked = true;
        search.changeMaxDistance(150000);
        break;
      case "200":
        document.getElementById("distanceMeterMiddle-radio").checked = true;
        search.changeMaxDistance(200000);
        break;
      case "250":
        document.getElementById("distanceMeterHigh-radio").checked = true;
        search.changeMaxDistance(250000);
        break;
      default:
          document.getElementById("distanceMeterManualInput").value = distance;
          document.getElementById("distanceMeterManual-radio").checked = true;
          search.checkNewManualDistance();
    }
  }
};
