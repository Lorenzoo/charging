var route = {

  curRoute: null, // TODO: dependencies entfernen
  alternatives: null,
  allRoutes: undefined,
  popups: [],
  type: "car",
  maxDistance: 200000,

  init: function (curRoute, alternatives, type) {
    if (type === undefined) {
      type = "car";
    }

    this.removeMarkers(true);

    route.type = type;
    this.allRoutes = {};
    route.allRoutes.curRoute = curRoute;
    for (var i = 0; i < alternatives.length; i++) {
      route.allRoutes["alternative" + i] = alternatives[i];
    }

    this.removePopups(route.allRoutes);

    this.determinRoutesDistincMiddle();
    this.waySearchStations();
  },

  altClicked: function (e) {
    this.removeMarkers();
    this.removePopups(route.allRoutes);

    Object.keys(route.allRoutes).forEach(function(key) {
      if (route.allRoutes[key].routesIndex == e.target._route.routesIndex) {
        route.allRoutes[key].choosenWay = true;
      } else {
        route.allRoutes[key].choosenWay = false;
      }
    });

    this.addWay();
  },

  waySearchStations: function () {
    var stationsExist = false;
    for (var i = 0; i < stationResources[route.type].data.length; i++) {
      var e = stationResources[route.type].data[i];
      if ( !StationLayer._hasSelectedSocket(e)
          || e.tags.access == 'private'
          || e.tags.access == 'customers' ) {
        continue;
      }

      Object.keys(route.allRoutes).forEach(function(key) {
        if (route.allRoutes[key].stations === undefined) {
          route.allRoutes[key].stations = [];
        }
        if (route.findCoordinateWithinRange(e, route.allRoutes[key].coordinates, 300)) {
          route.allRoutes[key].stations.push(Object.assign({}, e));
          stationsExist = true;
        }
      });
    }

    if (stationsExist) {
      rate.rateRoutes();
    } else if (StationLayer._isOneSocketEnabled()) {
      //  socket selected but no station in range
      route.addPopups({curRoute: route.allRoutes.curRoute});
      // setting the routes within lrm is necessary, because L.Routing.Control.onZoomEnd()
      // calls _updateLineCallback() which calls _updateLineCallback() using
      // leafletRoutingMachine._routes as parameter
      // this way routes would be visible that should not be:
      // e.g. alternatives where no stationSequence could be found
      leafletRoutingMachine._routes = [route.allRoutes.curRoute];
      leafletRoutingMachine._updateLines({route: route.allRoutes.curRoute, alternatives: []}, true);
      NotificationBar.update();
    }
  },

  findCoordinateWithinRange: function (point, crowd, range, left, right) {
    if (left === undefined) {
      left = 0;
    }
    if (right === undefined) {
      right = crowd.length - 1;
    }
    var middle = Math.floor(left + (right - left) / 2);

    if (right == left + 1) {
      var leftMiddle = left;
      var rightMiddle = right;
    } else {
      var leftMiddle = Math.floor(left + (middle - left) / 2);
      var rightMiddle = Math.floor(middle + (right - middle) / 2);
    }

    var latlng = L.latLng(point.lat, point.lon);
    var dLeftMiddle = latlng.distanceTo(crowd[leftMiddle]);
    var dRightMiddle = latlng.distanceTo(crowd[rightMiddle]);

    if (dLeftMiddle <= range || dRightMiddle <= range) {
      return true;
    } else if (right != left + 1) {
      if (dLeftMiddle < dRightMiddle) {
        return route.findCoordinateWithinRange(point, crowd, range, left, middle);
      } else {
        return route.findCoordinateWithinRange(point, crowd, range, middle, right);
      }
    } else {
      return false;
    }
  },

  chooseBestWay: function () {
    if (route.allRoutes === undefined) {
      return;
    }

    var choosenWayKey;
    Object.keys(route.allRoutes).forEach(function(key) {
      if (! choosenWayKey || route.allRoutes[key].rate < route.allRoutes[choosenWayKey].rate ) {
        choosenWayKey = key;
      }
      route.allRoutes[key].choosenWay = false;
    });
    route.allRoutes[choosenWayKey].choosenWay = true;

    route.addWay();
  },

  addWay: function () {
    var newAlternatives = [], choosenWayKey;
    Object.keys(route.allRoutes).forEach(function(key) {
      if (!route.allRoutes[key].choosenWay && route.allRoutes[key].choosenStations.length != 0) {
        newAlternatives.push(route.allRoutes[key]);
      }
      route.addPopups({key: route.allRoutes[key]});
      if (route.allRoutes[key].choosenWay) {
        choosenWayKey = key;
      }
    });

    // setting the routes within lrm is necessary, because L.Routing.Control.onZoomEnd()
    // calls _updateLineCallback() which calls _updateLines() using
    // leafletRoutingMachine._routes as parameter
    // this way routes would be visible that should not be:
    // e.g. alternatives where no stationSequence could be found
    leafletRoutingMachine._routes = [route.allRoutes[choosenWayKey]].concat(newAlternatives);

    leafletRoutingMachine._updateLines({route: route.allRoutes[choosenWayKey], alternatives: newAlternatives}, true);
    route.addMarkers(choosenWayKey);
    NotificationBar.update();
  },

  findPath: function (way) {
    var graph = route.createGraph(way);

    dijkstra = new Dijkstra(graph);
    var result = dijkstra.findShortestPath("start", "end");
    way.choosenStations = [];
    if (result === null) {
      console.log("keine passende Abfolge an Stationen gefunden");
      way.stationSequenceFound = false;
    } else {
      for (var i = 0; i < result.length; i++) {
        if (result[i] === "start" || result[i] === "end") {
          continue;
        }
        for (var k = 0; k < way.stations.length; k++) {
          if (way.stations[k].id == parseInt(result[i])) {
            way.choosenStations.push(way.stations[k]);
          }
        }
      }
      way.stationSequenceFound = true;
    }
  },

  createGraph: function (way) {
    this.calculateDistanceToStart(way);
    var graph = {};

    var nodes = [];
    // start
    nodes.push({
      id: "start",
      latLng: way.waypoints[0].latLng,
      rate: 1000,
      nearestRouteNode: 0,
      detour: {
        addDistance: 0,
        addTime: 0
      }
    });
    // relevant stations
    for (var i = 0; i < way.stations.length; i++) {
      var e = way.stations[i];
      nodes.push({
        id: e.id,
        latLng: L.latLng(e.lat, e.lon),
        rate: e.rateRoute,
        nearestRouteNode: e.nearestRouteNode,
        detour: e.detour
      });
    }
    // end
    nodes.push({
      id: "end",
      latLng: way.waypoints[1].latLng,
      rate: 0,
      nearestRouteNode: way.coordinates.length - 1,
      detour: {
        addDistance: 0,
        addTime: 0
      }
    });

    for (var i = 0; i < nodes.length - 1; i++) {
      for (var k = i + 1; k < nodes.length; k++) {
        var distance = Math.abs(
            (way.coordinates[nodes[i].nearestRouteNode].distanceToStart + nodes[i].detour.addDistance)
            -
            (way.coordinates[nodes[k].nearestRouteNode].distanceToStart + nodes[k].detour.addDistance)
        );
        if (distance <= this.maxDistance) {
          if (graph[nodes[i].id] === undefined) {
            graph[nodes[i].id] = {};
          }
          if (graph[nodes[k].id] === undefined) {
            graph[nodes[k].id] = {};
          }

          if (nodes[i].id === "start" && nodes[k].id === "end") {
            graph[nodes[i].id][nodes[k].id] = 1000;
          } else {
            graph[nodes[i].id][nodes[k].id] = nodes[k].rate;
            graph[nodes[k].id][nodes[i].id] = nodes[i].rate;
          }
        }
      }
    }

    return graph;
  },

  addMarkers: function (choosenWayKey) {
    Object.keys(route.allRoutes).forEach(function(key) {
      if (!route.allRoutes[key].stationSequenceFound && !route.allRoutes[key].choosenWay) {
        return;
      }
      route.allRoutes[key].markers = [];

      for (var i = 0; i < route.allRoutes[key].stations.length; i++) {
        var curStation = route.allRoutes[key].stations[i];
        var newMarker = {};

        if (route.allRoutes[key].choosenStations.indexOf(curStation) !== -1
            && route.allRoutes[key].choosenWay) {
          newMarker = L.marker (
            L.latLng(curStation.lat, curStation.lon),
            {
              icon: L.icon (
                {
                  iconUrl: 'svg/pointers/pointer_recommended.svg',
                  iconSize: [47, 70],
                  iconAnchor: [20, 65],
                  className: 'choosen-marker'
                }
              )
            }
          ).on('mousedown', addStationInfo);
        } else {
          newMarker = L.marker (
            L.latLng(curStation.lat, curStation.lon),
            {
              icon: route._getMarkerIcon(curStation, key != choosenWayKey)
            }
          ).on('mousedown', addStationInfo);
        }

        newMarker.tags = curStation.tags;
        newMarker.id = curStation.id;
        newMarker.rateRoute = curStation.rateRoute;
        route.allRoutes[key].markers.push(newMarker);
        StationLayer._removeMarker(curStation);
        map.addLayer(newMarker);
      }
    });
  },

  _getMarkerIcon: function (poi, alternative) {
    var url, size, anchor, cssClass, rate;
    rate = poi.rate ? poi.rate : poi.rateRoute;

    if (alternative) {
      if (rate < 1.6) {
        url = 'svg/pointers/pointer.svg';
        size = [47, 70];
        anchor = [23.5, 70];
        cssClass = 'alternative-route-marker';
      } else if (rate < 2.6) {
        url = 'svg/pointers/pointer1.svg';
        size = [40, 60];
        anchor = [20, 60];
        cssClass = 'alternative-route-marker1';
      } else if (rate < 3.6) {
        url = 'svg/pointers/pointer2.svg';
        size = [37, 55];
        anchor = [17.5, 55];
        cssClass = 'alternative-route-marker2';
      } else if (rate < 4.6) {
        url = 'svg/pointers/pointer3.svg';
        size = [30, 45];
        anchor = [15, 45];
        cssClass = 'alternative-route-marker3';
      } else {
        url = 'svg/pointers/pointer4.svg';
        size = [23, 35];
        anchor = [11.5, 35];
        cssClass = 'alternative-route-marker4';
      }
    } else {
      if (rate < 1.6) {
        url = 'svg/pointers/pointer.svg';
        size = [47, 70];
        anchor = [23.5, 70];
        cssClass = 'choosen-route-marker';
      } else if (rate < 2.6) {
        url = 'svg/pointers/pointer1.svg';
        size = [40, 60];
        anchor = [20, 60];
        cssClass = 'choosen-route-marker1';
      } else if (rate < 3.6) {
        url = 'svg/pointers/pointer2.svg';
        size = [37, 55];
        anchor = [17.5, 55];
        cssClass = 'choosen-route-marker2';
      } else if (rate < 4.6) {
        url = 'svg/pointers/pointer3.svg';
        size = [30, 45];
        anchor = [15, 45];
        cssClass = 'choosen-route-marker3';
      } else {
        url = 'svg/pointers/pointer4.svg';
        size = [23, 35];
        anchor = [11.5, 35];
        cssClass = 'choosen-route-marker4';
      }
    }

    icon = L.icon({
      iconUrl: url,
      shadowUrl: '',
      iconSize: size,
      iconAnchor: anchor,
      className: cssClass
    });
    return icon;
  },

  removeMarkers: function (hardRemove) {
    if (route.allRoutes === undefined) {
      return;
    }
    Object.keys(route.allRoutes).forEach(function(key) {
      if (route.allRoutes[key].markers !== undefined) {
        for (var i = 0; i < route.allRoutes[key].markers.length; i++) {
          var marker = route.allRoutes[key].markers[i];
          if (marker === undefined) {
            continue;
          }
          map.removeLayer(marker);
        }
        route.allRoutes[key].markers = [];
      }
      if (hardRemove) {
        route.allRoutes[key].stations = undefined;
      }
    });
  },

  determinRoutesDistincMiddle: function (routes) {
    if (routes === undefined) {
      routes = route.allRoutes;
    }

    var everyCoordinate = [];
    var popupRoutes = {};

    // --- fill array ---
    if (routes === undefined) {
      return;
    }
    Object.keys(routes).forEach(function(key) {
      popupRoutes[key] = {};
      popupRoutes[key].coordinates = [];
      popupRoutes[key].summary = routes[key].summary;
      for (var i = 0; i < routes[key].coordinates.length; i++) {
        var newKey = routes[key].coordinates[i].lat + "-" + routes[key].coordinates[i].lng
        var e = routes[key].coordinates[i];
        e.key = newKey;
        e.origin = key;
        everyCoordinate.push(e);
      }
    });

    // -- sort array --
    everyCoordinate.sort(compairKeys);

    // -- remove double entries --
    var counter, key, origin;
    for (var i = 0; i < everyCoordinate.length; i++) {
      if (key != everyCoordinate[i].key && counter == 1) {
        popupRoutes[everyCoordinate[i].origin].coordinates.push(everyCoordinate[i]);
      }
      if (key != everyCoordinate[i].key) {
        key = everyCoordinate[i].key;
        counter = 1;
        origin = everyCoordinate.origin;
      } else if (origin != everyCoordinate[i].origin) {
        counter ++;
      }
    }

    Object.keys(routes).forEach(function(key) {
      var middle = popupRoutes[key].coordinates[Math.floor(popupRoutes[key].coordinates.length / 2)];
      routes[key].distinctMiddle = middle;
    });


    function compairKeys (a, b) {
      if (a.key < b.key) {
        return -1;
      }
      if (a.key > b.key) {
        return 1;
      }
      // a must be equal to b
      return 0;
    }
  },

  calculateDistanceToStart: function (way) {
    var co = way.coordinates;
    if (! co) {
      return;
    }

    co[0].distanceToStart = 0;
    for (var i = 1; i < co.length; i++) {
      co[i].distanceToStart = Math.round(
        L.latLng(co[i].lat, co[i].lng).distanceTo([co[i - 1].lat, co[i - 1].lng]) + co[i - 1].distanceToStart
      );
    }
  },

  addPopups: function (routes) {
    if (routes === undefined) {
      routes = route.allRoutes;
    }

    Object.keys(routes).forEach(function(key) {
      var middle = routes[key].distinctMiddle

      var formatter = new L.Routing.Formatter;
      var time = "<span class='popup-time'>" + formatter.formatTime(routes[key].summary.totalTime) + "</span>";
      var sensitivity = (routes[key].summary.totalDistance / 1000 < 50) ? -1 : 0;
      var distance = "<span class='popup-distance'>" + formatter.formatDistance(routes[key].summary.totalDistance, sensitivity) + "</span>";

      var popup = L.popup({closeButton: false, closeOnClick: false, className: routes[key].choosenWay ? 'popup-route-summary-choosen no-selection' : 'popup-route-summary-alt no-selection'})
			.setLatLng([middle.lat, middle.lng])
			.setContent(time + distance)
			.addTo(map);
      // this update is necessary else margin-left of .popup-distance
      // would cause the popup to be displayed in two lines
      routes[key].popup = popup;
      popup.update();
      route.popups.push(popup);
      popup._container.onclick = function() {
        route.altClicked({'target': {'_route': {'routesIndex': routes[key].routesIndex}}});
      };
    });
  },

  removePopups: function () {
    for (var i = 0; i < route.popups.length; i++) {
      map.removeLayer(route.popups[i]);
    }

    route.popups = [];
  }
};
