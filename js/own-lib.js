var stationInfoRemoveable = true;
function addStationInfo(e) {
    // just a little animation fired when clicking on a marker
    var icon = e.target._icon;
    if (icon) {
      newIcon = e.target.options.icon.options;
      newIcon.className += " station-marker-divIcon-click";
      newIcon = L.divIcon(newIcon);
      pulseMarker = L.marker(e.target._latlng, {icon: newIcon}).on('mousedown', addStationInfo);
      pulseMarker.tags = e.target.tags;
      pulseMarker.id = e.target.id;
      pulseMarker.rate = e.target.rate;
      pulseMarker.aroundPOIs = e.target.aroundPOIs;
      map.addLayer(pulseMarker);
      setTimeout(function(pulseMarker) {
        map.removeLayer(pulseMarker);
      }, 500, pulseMarker);
    }

    stationInfoSwiper.slideTo(1);

    // center map on marker if the station info sidebar would overlap the marker
    var markerLeft = e.containerPoint.x;
    var stationLeft = window.innerWidth - document.getElementById("station-info").offsetWidth;
    // we don't center on the marker if the station info sidebar is fullscreen
    if (markerLeft > stationLeft && stationLeft != 0) {
      centerMapOnMarker(e.target);
    }

    // show surrunding POI
    if (map.getZoom() > 16) {
      zoom = 17;
    } else {
      zoom = 16;
    }
    document.getElementById('around-interest').onclick = function() { centerMapOnMarker(e.target, zoom); };

    // scroll to top
    document.getElementById('station-info').scrollTop = 0;

    tags = e.target.tags;

    var sockets = {
        'socket:type1' : {
            'title' : 'Type 1',
            'location' : 'svg/sockets/type1.svg'
        },
        'socket:type2' : {
            'title' : 'Type 2',
            'location' : 'svg/sockets/type2.svg'
        },
        'socket:type2_combo' : {
            'title' : 'Type 2 CCS',
            'location' : 'svg/sockets/type2-combo.svg'
        },
        'socket:combo2' : {
            'title' : 'Type 2 CCS',
            'location' : 'svg/sockets/type2-combo.svg'
        },
        'socket:chademo' : {
            'title' : 'Chademo',
            'location' : 'svg/sockets/chademo.svg'
        },
        'socket:tesla_standard' : {
            'title' : 'Tesla',
            'location' : 'svg/sockets/tesla.svg'
        },
        'socket:tesla_supercharger' : {
            'title' : 'Tesla',
            'location' : 'svg/sockets/tesla.svg'
        },
        'socket:schuko' : {
            'title' : 'Schuko',
            'location' : 'svg/sockets/schuko.svg'
        },
        'socket:cee_blue' : {
            'title' : 'blue CEE',
            'location' : 'svg/sockets/cee_blue.svg'
        },
        'socket:cee_red_16a' : {
            'title' : 'red CEE 16A',
            'location' : 'svg/sockets/cee_red_16a.svg'
        },
        'socket:cee_red_32a' : {
            'title' : 'red CEE 32A',
            'location' : 'svg/sockets/cee_red_32a.svg'
        },
        'unknown' : {
            'title' : 'unknown',
            'location' : 'svg/sockets/unknown.svg'
        }
    };

    // title
    if (e.target.tags.name == undefined) {
        document.getElementById("station-title").innerHTML = Locals.getText('station-info', 'title');
    } else {
        document.getElementById("station-title").innerHTML = e.target.tags.name;
    }

    // access
    var accessHTML = document.getElementById("station-access");
    accessHTML.style.display = "block";
    if (tags.access == "customers") {
      accessHTML.innerHTML = Locals.getText("access", "customers");
    } else if (tags.access == "private") {
      accessHTML.innerHTML = Locals.getText("access", "private");
    } else {
      accessHTML.style.display = "none";
    }

    // description
    //      currently only supports english and german descriptions
    var descriptionTags = [
        "description",
        "description:en",
        "description:de",
        "note",
        "note:en",
        "note:de",
    ];
    document.getElementById("station-description").style.display = "block";
    for (i = 0; i < descriptionTags.length; i++) {
        if (e.target.tags[descriptionTags[i]] != undefined) {
            document.getElementById("station-description").innerHTML = e.target.tags[descriptionTags[i]];
            break;
        } else if (i  == descriptionTags.length - 1) {
            document.getElementById("station-description").innerHTML = ""
            document.getElementById("station-description").style.display = "none";
        }
    }

    // rate
    var div = document.getElementById("station-rate");
    div.innerHTML = "";
    div.style.display = "none";

    if (e.target.rate || e.target.rateRoute) {
      var rate = e.target.rate ? e.target.rate : e.target.rateRoute;
      var rate = rate - 1; // remove base value
      var maxRate = 5;

      // full starts
      var full = maxRate - Math.ceil(rate);
      for (var i = 0; i < full; i++) {
        div.innerHTML += "<span class='fa fa-star fa-lg'></span>";
      }

      // half star
      var empty = maxRate - full;
      if ((rate - Math.floor(rate)) < 0.6
        && (rate - Math.floor(rate)) != 0) {
        div.innerHTML += "<span class='fa fa-star-half-o fa-lg'></span>";
        empty --;
      }

      // empty star
      for (var i = 0; i < empty; i++) {
        div.innerHTML += "<span class='fa fa-star-o fa-lg'></span>";
      }

      div.style.display = "block";
    }

    // vehicles
    document.getElementById("vehicle-description-car").style.backgroundColor = "white";
    document.getElementById("vehicle-description-car").style.color = "black";
    document.getElementById("vehicle-description-truck").style.backgroundColor = "white";
    document.getElementById("vehicle-description-truck").style.color = "black";
    document.getElementById("vehicle-description-motorcycle").style.backgroundColor = "white";
    document.getElementById("vehicle-description-motorcycle").style.color = "black";
    document.getElementById("vehicle-description-bicycle").style.backgroundColor = "white";
    document.getElementById("vehicle-description-bicycle").style.color = "black";
    if (e.target.tags.car == "yes") {
        document.getElementById("vehicle-description-car").style.backgroundColor = "#009fe3";
        document.getElementById("vehicle-description-car").style.color = "white";
    }
    if (e.target.tags.truck == "yes") {
        document.getElementById("vehicle-description-truck").style.backgroundColor = "#009fe3";
        document.getElementById("vehicle-description-truck").style.color = "white";
    }
    if (e.target.tags.scooter == "yes") {
        document.getElementById("vehicle-description-motorcycle").style.backgroundColor = "#009fe3";
        document.getElementById("vehicle-description-motorcycle").style.color = "white";
    }
    if (e.target.tags.bicycle == "yes" || e.target.tags.bike == "yes") {
        document.getElementById("vehicle-description-bicycle").style.backgroundColor = "#009fe3";
        document.getElementById("vehicle-description-bicycle").style.color = "white";
    }

    // charging fee
    // TODO: add support for free=* tag - invers to fee=*
    colorUnknown = "#cecece";
    colorCosts = "#ff644a";
    colorGratis = "#dbff4a";
    chargingFee = document.getElementById("charging-fee");
    if (e.target.tags.fee == undefined) {
        chargingFee.innerHTML = Locals.getText('charging-fee', 'unknown');
        chargingFee.style.backgroundColor = colorUnknown;
    } else if (e.target.tags.fee == "yes") {
        chargingFee.innerHTML = Locals.getText('charging-fee', 'yes');
        chargingFee.style.backgroundColor = colorCosts;
    } else if (e.target.tags.fee == "no" || e.target.tags["payment:none"] == "yes") {
        chargingFee.innerHTML = Locals.getText('charging-fee', 'no');
        chargingFee.style.backgroundColor = colorGratis;
    }
    //parking fee
    parkingFee = document.getElementById("parking-fee");
    if (e.target.tags["parking:fee"] == undefined) {
        parkingFee.innerHTML = Locals.getText('parking-fee', 'unknown');
        parkingFee.style.backgroundColor = colorUnknown;
    } else if (e.target.tags["parking:fee"] == "yes") {
        parkingFee.innerHTML = Locals.getText('parking-fee', 'yes');
        parkingFee.style.backgroundColor = colorCosts;
    } else if (e.target.tags["parking:fee"] == "no") {
        parkingFee.innerHTML = Locals.getText('parking-fee', 'no');
        parkingFee.style.backgroundColor = colorGratis;
    }

    // sockets
    socketSection = document.getElementById("station-sockets");
    // remove old sockets
    while (child = socketSection.lastChild) {
        socketSection.removeChild(child);
    }
    for (tag in tags) {
        // here we have to use a regex expression because some old IE versions don't support startsWith()
        // tag.startsWith("socket:")  <==>  /^socket:/.test(tag)
        if (hasOwnProperty.call(tags, tag) && /^socket:/.test(tag) && (tag.match(/:/g)||[]).length == 1) {

            newSocket = document.createElement("div");
            newSocket.className += "new-socket";
            socketSection.appendChild(newSocket);
            // count
            count = document.createElement("span");
            count.innerHTML = tags[tag] + "x";
            count.className += "socket-count";
            newSocket.appendChild(count);
            // name
            title = document.createElement("span");
            if (sockets[tag] == undefined) {
                title.innerHTML = sockets["unknown"].title;
            } else {
                title.innerHTML = sockets[tag].title;
            }
            title.className += "socket-title";
            newSocket.appendChild(title);
            // svg icon
            // note: using img tag here because svgs seem to load
            // much faster in Firefox 52 when using img instead of object
            svgImage = document.createElement("img");
            if (sockets[tag] == undefined) {
                svgImage.src = sockets["unknown"].location;
            } else {
                svgImage.src = sockets[tag].location;
            }
            svgImage.className += "svg-socket";
            newSocket.appendChild(svgImage);
            // power
            power = document.createElement("span");
            power.innerHTML = "22 kW";
            power.className += "socket-power";
            newSocket.appendChild(power);
            // voltage + amperage
            va = document.createElement("span");
            va.className += "socket-va";
            newSocket.appendChild(va);
            // voltage
            voltage = document.createElement("div");
            voltage.className += "socket-voltage";
            if (tags[tag + ":voltage"] != undefined) {
                voltage.innerHTML = tags[tag + ":voltage"];
            } else {
                voltage.innerHTML = "? V";
                voltage.className += "socket-voltage-red";
            }
            va.appendChild(voltage);
            //amperage
            amperage = document.createElement("div");
            amperage.className += "socket-amperage";
            if (tags[tag + ":amperage"] != undefined) {
                amperage.innerHTML = tags[tag + ":amperage"];
            } else {
                amperage.innerHTML = "? A";
                amperage.className += "socket-amperage-red";
            }
            va.appendChild(amperage);
        }
    }

    //opening hours
    // this part of code was taken by the opening_hours.js osm evaluation tool website
    // http://openingh.openstreetmap.de/evaluation_tool/
    // https://github.com/opening-hours/opening_hours.js/blob/master/js/opening_hours_table.js
    if (e.target.tags["opening_hours"] != undefined) {
        document.getElementById("opening-hours").style.display = "flex";
        var ohHTMLElement   = document.getElementById("opening-interval");
        // remove old opening hours
        while (child = ohHTMLElement.lastChild) {
            ohHTMLElement.removeChild(child);
        }
        var oh              = new opening_hours(e.target.tags["opening_hours"], null, { locale: 'de' });
        var it_date         = new Date();
        it_date.setDate(it_date.getDate() - getDayOfWeek(it_date))
        var it              = oh.getIterator(it_date);
        var hasNextChange   = it.advance();
        var date_today = new Date();
        date_today.setHours(0, 0, 0, 0);
        var date 			= new Date(date_today);
        date.setDate(date.getDate() - getDayOfWeek(date));

        for (var i = 0; i < 7; i++) {
            it.setDate(date);
            var dayHTMLElement = document.createElement("div");
            var isOpen      = it.getState();
            var unknown     = it.getUnknown();
            var stateString = it.getStateString(false);
            var prevdate    = date;
            var curdate     = date;
            var alwaysOpen  = true;
            while (hasNextChange && it.advance() && curdate.getTime() - date.getTime() < 24*60*60*1000) {
                alwaysOpen  = false;
                curdate     = it.getDate();
                var from    = prevdate.getTime() - date.getTime();
                var to      = curdate.getTime() - date.getTime();

                if(to > 24*60*60*1000) {
                    to  = 24*60*60*1000;
                }

                from    *= 100/1000/60/60/24;
                to      *= 100/1000/60/60/24;

                var timeSlotHTMLElement = document.createElement("div");
                if (isOpen == true) {
                    timeSlotHTMLElement.className += " hours-open";
                    if (date.getDate() == date_today.getDate()) {
                        timeSlotHTMLElement.className += " hours-today-for";
                    }
                } else if (unknown == true) {
                    timeSlotHTMLElement.className += " hours-unknown";
                } else {
                    timeSlotHTMLElement.className += " hours-closed";
                    if (date.getDate() == date_today.getDate()) {
                        timeSlotHTMLElement.className += " hours-today-back";
						var tmpStop = new Date();
						tmpStop.setHours(0, 0, 0, 0);
						tmpStop.setDate(tmpStop.getDate() + 1);
						var tmpIt = oh.getIterator(date_today);
						var isOpenToday = tmpIt.advance(tmpStop);
                        if (isOpenToday == false) {
                            timeSlotHTMLElement.className += " hours-today-closed";
                        }
                    }
                }
                timeSlotHTMLElement.style.width = (to - from) + "%";
                //timeSlotHTMLElement.innerHTML = date.getDate() + "  " + getDayOfWeek(date);
                dayHTMLElement.appendChild(timeSlotHTMLElement);

                prevdate    = curdate;
                isOpen      = it.getState();
                unknown     = it.getUnknown();
                stateString = it.getStateString(false);
            }
            if (!hasNextChange && alwaysOpen) { // 24/7
                var timeSlotHTMLElement = document.createElement("div");
                timeSlotHTMLElement.className += " hours-open";
                timeSlotHTMLElement.style.width = "100%";
                if (date.getDate() == date_today.getDate()) {
                    timeSlotHTMLElement.className += " hours-today-for";
                }
                dayHTMLElement.appendChild(timeSlotHTMLElement);
            }
            ohHTMLElement.appendChild(dayHTMLElement);
            date.setDate(date.getDate() + 1);
        }
    } else {
        document.getElementById("opening-hours").style.display = "none";
    }

    // auth methods
    var authMethods = document.getElementById("auth-methods-inner");
    document.getElementById("authentification-methods").style.display = "none";
    // remove old auth methods
    while (child = authMethods.lastChild) {
        authMethods.removeChild(child);
    }

    for (tag in tags) {
      if (! tag.indexOf("authentication:") == 0 || tags[tag] == "no") {
        continue;
      }
      document.getElementById("authentification-methods").style.display = "flex";

      tag = tag.replace("authentication:", "");

      newAuth = document.createElement("div");
      newAuth.className += "auth-method";

      switch (tag) {
        case "none":
          newAuth.innerHTML =
            '<span class="fa fa-ban fa-fw"></span> ' + Locals.getText('auth-methods', 'none');
          break;
        case "phone_call":
          newAuth.innerHTML =
            '<span class="fa fa-phone-square fa-fw"></span> ' + Locals.getText('auth-methods', 'phone_call');
          break;
        case "sms":
        case "short_message":
          newAuth.innerHTML =
            '<span class="fa fa-envelope-open fa-fw"></span> ' + Locals.getText('auth-methods', 'short_message');
          break;
        case "nfc":
          newAuth.innerHTML =
            '<span class="fa fa-rss fa-fw"></span> ' + Locals.getText('auth-methods', 'nfc');
          break;
        case "money_card":
          newAuth.innerHTML =
            '<span class="fa fa-credit-card fa-fw"></span> ' + Locals.getText('auth-methods', 'money_card');
          break;
        case "debit_card":
          newAuth.innerHTML =
            '<span class="fa fa-credit-card-alt fa-fw"></span> ' + Locals.getText('auth-methods', 'debit_card');
          break;
        case "membership_card":
          newAuth.innerHTML =
            '<span class="fa fa-id-card-o fa-fw"></span> ' + Locals.getText('auth-methods', 'membership_card');
          break;
        default:
          if (tag == "phone_call:number" || tag == "short_message:number" || tag == "short_message:text" || tags["authentication:" + tag] == "no") {
            break;
          }
          newAuth.innerHTML =
            '<span class="fa fa-question fa-lg"></span> ' + tag + ' ' + Locals.getText('auth-methods', 'unknown');
      }
      if (newAuth.innerHTML) {
        authMethods.appendChild(newAuth);
      }
    }

    // payment methods
    var paymentMethods = document.getElementById("payment-methods-inner");
    document.getElementById("payment-methods").style.display = "none";
    // remove old auth methods
    while (child = paymentMethods.lastChild) {
        paymentMethods.removeChild(child);
    }

    for (tag in tags) {
      if (! tag.indexOf("payment:") == 0 || tags[tag] == "no" || tag == "payment:none") {
        continue;
      }
      document.getElementById("payment-methods").style.display = "flex";

      tag = tag.replace("payment:", "");

      newPayment = document.createElement("div");
      newPayment.className += "payment-method";

      switch (tag) {
        case "cash":
        case "notes":
        case "coins":
          newPayment.innerHTML =
            '<span class="fa fa-money fa-fw"></span> ' + Locals.getText('payment-methods', 'cash');
          break;
        case "debit_cards":
        case "maestro":
          newPayment.innerHTML =
            '<span class="fa fa-credit-card-alt fa-fw"></span> ' + Locals.getText('payment-methods', 'debit_cards');
          break;
        case "credit_cards":
          newPayment.innerHTML =
            '<span class="fa fa-credit-card fa-fw"></span> ' + Locals.getText('payment-methods', 'credit_cards');
          break;
        case "sms":
        case "short_message":
          newPayment.innerHTML =
            '<span class="fa fa-envelope-open fa-fw"></span> ' + Locals.getText('payment-methods', 'short_message');
          break;
        default:
          if (tags["payment:" + tag] == "no") {
            break;
          }
          newPayment.innerHTML =
            '<span class="fa fa-question fa-lg"></span> ' + tag + ' ' + Locals.getText('payment-methods', 'unknown');
      }
      if (newPayment.innerHTML) {
        paymentMethods.appendChild(newPayment);
      }
    }


    // info about missing data
    showMissingDataInfo(e);

    urlHash.onChange(e.target.id);

    // because the maps mousedown event (used to remove info bar)
    // will also be triggert when the markers mousedown (used to add info bar)
    // is triggert, we have to make sure the info bar will not flicker
    // or disapear by clicking on a marker
    stationInfoRemoveable = false;
    setTimeout(function() {
      stationInfoRemoveable = true;
    }, 200);

    // start css transition
    // the 30ms delay helps avoiding surge
    setTimeout(function() {
      document.getElementById("move-station-info").checked = true;
    }, 30);
}

function showMissingDataInfo (e) {
  var visible = false,
      tags = e.target.tags;

  if (tags.car == undefined && tags.truck == undefined && tags.scooter ==  undefined && tags.bicycle == undefined && tags.bike == undefined) {
    visible = true;
  }

  if (tags.fee == undefined || tags["parking:fee"] == undefined) {
    visible = true;
  }

  if (! StationLayer._hasSelectedSocket(e.target)) {
    visible = true;
  }

  if (tags["opening_hours"] == undefined) {
    visible = true;
  }

  if (! isPaymentSpecified(tags)) {
    visible = true;
  }

  if (! isAuthSpecified(tags)) {
    visible = true;
  }

  if (visible) {
    document.querySelector("#missing-data p").innerHTML = Locals.getText('missing-data', 'missing-text');
    document.querySelector("#missing-data a").innerHTML = Locals.getText('missing-data', 'add');
    document.querySelector("#missing-data a").onclick = function () { Editor.show(e.target.id); };
    document.getElementById("missing-data").style.display = "block";
  } else {
    document.getElementById("missing-data").style.display = "none";
  }
}

function isPaymentSpecified (tags) {
  for (tag in tags) {
    // no payment tag necessary when fee=no
    if (tag.indexOf("payment:") != -1 || tags.fee == "no") {
      return true;
    }
  }
  return false;
}

function isAuthSpecified (tags) {
  for (tag in tags) {
    // no payment tag necessary when fee=no
    if (tag.indexOf("authentication:") != -1) {
      return true;
    }
  }
  return false;
}

function removeStationInfo(e) {
  if (stationInfoRemoveable) {
    urlHash.onChange(0);
  	document.getElementById("move-station-info").checked = false;
    if (stationInfoSwiper.activeIndex != 0) {
      stationInfoSwiper.slideTo(0);
    }
  }
}

function setSockets() {
	// deselcet all sockets
	socketInputs = document.querySelectorAll('.socket-chbox');
	for (var i=0; i<socketInputs.length; i++) {
		socketInputs[i].checked = false;
	}
	// TODO select all sockets from url
}

// function returns day of week - starting with mo 0, tu 1, we 2 ...
// getDay() starts with su 0, mo 1 ...
function getDayOfWeek(date){
    var date = new Date(date);
    var dayOfWeek = date.getDay();
    if (dayOfWeek == '0') {
        dayOfWeek = 6;
    } else {
        dayOfWeek -= 1;
    }
    return dayOfWeek;
}

// zoom control
function zoomIn() {
    locate.disableCenteringOnMarker();
    map.setZoom(map.getZoom() + 1)
}

function zoomOut() {
    locate.disableCenteringOnMarker();
    map.setZoom(map.getZoom() - 1)
}

function centerMapOnMarker(target, zoom) {
  if (zoom === undefined) {
    zoom = map.getZoom();
  }
  // the station values below are longitude values
  var stationInfoLeft = map.unproject([window.innerWidth - document.getElementById("station-info").offsetWidth, 0], zoom).lng;
  var stationInfoRight = map.unproject([window.innerWidth, 0], zoom).lng;
  var stationInfoWidth = (stationInfoRight - stationInfoLeft);
  var markerLocation = target.getLatLng();
  var newMapMiddle = [markerLocation.lat, markerLocation.lng + stationInfoWidth / 2];

  setTimeout(function() {
    map.setView(newMapMiddle, zoom, {animation: true});
  }, 100);
}

function setTileLayer () {
  var satellite = document.getElementById("satellite-layer-chbox").checked;
  var traffic = document.getElementById("traffic-layer-chbox").checked;
  if (!satellite && !traffic) {
    tileLayer.setUrl(tileLayerURL.streets);
  } else if (!satellite && traffic) {
    tileLayer.setUrl(tileLayerURL.streetsTraffic);
  } else if (satellite && !traffic) {
    tileLayer.setUrl(tileLayerURL.satellite);
  } else if (satellite && traffic) {
    tileLayer.setUrl(tileLayerURL.satelliteTraffic);
  }
}
