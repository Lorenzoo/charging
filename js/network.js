var network = {

  getJSON: function (file, self, onSuccessCallback, onFailureCallback) {
    network.makeAjaxRequest(file, self, onSuccessCallback, onFailureCallback);
  },

  overpassBusy: false,
  overpassQueue: undefined,
  makeOverPassRequest: function (query, self, callback) {
    var url, nextRequest;
    if (query) {
      url = "https://overpass-api.de/api/interpreter?data=[out:json];" + query;
      nextRequest = network.makeAjaxRequest.bind(this, url, self,
        function (json, self) {
          network.makeOverPassRequest();
          callback(json, self);
        },
        function () {
          document.getElementById('loading-bar').style.background='red';
          network.overpassBusy = false;
        });

      if (network.overpassBusy) {
        network.overpassQueue = nextRequest;
        return;
      }
    } else if (network.overpassQueue) {
      nextRequest = network.overpassQueue;
      network.overpassQueue = undefined;
    }

    if (nextRequest) {
      network.overpassBusy = true;
      document.getElementById('loading-bar').style.background='transparent';
      ! network.nanobarVisible && network.showNanobar();
      nextRequest();
    } else {
      network.overpassBusy = false;
    }
  },

  osrmBusy: false,
  makeOSRMRequest: function (query, self, onSuccessCallback, onFailureCallback) {
    var serviceURL = 'https://api.mapbox.com/directions/v5/mapbox/driving/',
    // https://router.project-osrm.org/route/v1/driving/
    url = serviceURL + query +
            '&access_token=pk.eyJ1Ijoib3NtdGVzdGluZyIsImEiOiJjajB3ZTZvY2gwMGVjMndwb25nNTNwbWxwIn0.gaKos5HcsbCJ0JMj_5Nz0w';
    network.makeAjaxRequest(url, self, onSuccessCallback, onFailureCallback);
  },

  makeAjaxRequest: function (url, self, onSuccessCallback, onFailureCallback) {
    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function() {
  		if (this.readyState == 4 && this.status >= 200 && this.status < 400) {
        onSuccessCallback(JSON.parse(this.response), self);
      }

      if (this.readyState == 4 && this.status >= 400 && onFailureCallback !== undefined) {
  		  onFailureCallback(undefined, self);
      }
  	};

    if (loadingScreen.isVisible()) {
      xhttp.onprogress=function (e) { loadingScreen.updatePercentage(e); };
    }

    xhttp.open("GET",url,true);
  	xhttp.send();

  },

  setupAroundRequest: function (relevantStations, callback, self, overwrite) {
    var query;

    // if we only get one station convert it into an array
    if (relevantStations.id !== undefined) {
      relevantStations = [ relevantStations ];
    }

    // only add the ids to the query if we didn't already retrieve the aroundPOIs
    for (var i = 0; i < relevantStations.length; i++) {
        if (relevantStations[i].aroundPOIs === undefined || overwrite === true) {
          var position = relevantStations[i].lat + "," + relevantStations[i].lon;
          query = ((query === undefined) ? '' : query) +
                      "node(" + relevantStations[i].id + ");";
        }
    }
    if (query !== undefined) {
      // leading bracket of union
      query = "(" + query;
      // tailing bracket of union + filters
      query = query + ')->.a; node[amenity=fast_food](around.a:300);out; node[amenity=restaurant](around.a:300);out; node[amenity=cafe](around.a:300);out;';
      network.makeOverPassRequest(query, self,
        function (json, that) {
          network.processAroundResponse(json, relevantStations);
          callback(relevantStations, that);
        }
      )
    } else {
      callback(relevantStations, self);
    }

  },

  processAroundResponse: function (response, relevantStations) {
    if (response === undefined) {
      return;
    }

    for (var i = 0; i < relevantStations.length; i++) {
      var latLng = L.latLng(relevantStations[i].lat, relevantStations[i].lon);
      for(var k = 0; k < response.elements.length; k++) {
        var e = response.elements[k];
        if (300 >= latLng.distanceTo([e.lat, e.lon])) {
          if (relevantStations[i].aroundPOIs === undefined) {
            relevantStations[i].aroundPOIs = [];
          }
          relevantStations[i].aroundPOIs.push(e);
        }
      }

      // no POI around this stations
      // to avoid querying this station again and again we set null
      if (relevantStations[i].aroundPOIs === undefined) {
        relevantStations[i].aroundPOIs = null;
      }
    }

    StationLayer.refreshStationsAround();
  },

  nanobarVisible: false,
  showNanobar: function () {
      if (network.overpassBusy || network.osrmBusy) {
        network.nanobarVisible = true;
        nanobar.go(100);
        setTimeout(network.showNanobar, 1400);
      } else {
        network.nanobarVisible = false;
      }
  }
}
