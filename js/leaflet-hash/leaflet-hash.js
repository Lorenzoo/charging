(function(window) {
	var HAS_HASHCHANGE = (function() {
		var doc_mode = window.documentMode;
		return ('onhashchange' in window) &&
			(doc_mode === undefined || doc_mode > 7);
	})();

	L.Hash = function(map) {
		this.onHashChange = L.Util.bind(this.onHashChange, this);

		if (map) {
			this.init(map);
		}
	};

	L.Hash.parseHash = function(hash) {
		if(hash.indexOf('#') === 0) {
			hash = hash.substr(1);
		}
		var args = hash.split("/");
		if (args.length >= 3 && args.length <= 7) {
			// map location
			var zoom = parseInt(args[0], 10),
			lat = parseFloat(args[1]),
			lon = parseFloat(args[2]);
			// vehicle type, active sockets, selected station
			for (var i = 3; i < args.length; i++) {
				L.Hash.parseHashDetail(args[i]);
			}

			if (isNaN(zoom) || isNaN(lat) || isNaN(lon)) {
				return false;
			} else {
				return {
					center: new L.LatLng(lat, lon),
					zoom: zoom
				};
			}
		} else {
      // default map view if no url hash is provided
      return {
				center: new L.LatLng(51.331, 10.107),
				zoom: 6
      };
		}
	};

	L.Hash.socketSelectionSwiperHorizontalStart;
	L.Hash.parseHashDetail = function(str) {
		if (str.indexOf("sockets:") === 0) {
			var sockets = str.substr(8);
			sockets = sockets.split(",");
			L.Hash.activeSockets = sockets;
			for (var k = 0; k < sockets.length; k++) {
				if (StationLayer.sockets.hasOwnProperty(sockets[k])) {
					StationLayer.sockets[sockets[k]] = true;
				}
			}
		} else if (str.indexOf("station:") === 0) {
			var stationId = str.substr(8);
			StationLayer.options.selectedStation = stationId;
		} else if (str.indexOf("type:") === 0) {
			var type = str.substr(5);
			L.Hash.vehicleType = type;
			StationLayer.options.type = type;
			// set vehicle in socket selection menu
			vehicleChbox = document.querySelectorAll(".vehicle-selection-chbox");
			for (var i = 0; i < vehicleChbox.length; i++) {
				if (vehicleChbox[i].id.indexOf(type) !== -1) {
					vehicleChbox[i].checked = true;
					L.Hash.socketSelectionSwiperHorizontalStart = i;
					break;
				}
			}
		} else if (str.indexOf("maxDistance:") === 0) {
			var distance = str.substr(12);
			L.Hash.maxDistance = distance;
			search.setDistanceByHash(distance);
		}
	};

	L.Hash.vehicleType = "car";
	L.Hash.activeSockets = [];
	L.Hash.selectedStation = undefined;
	L.Hash.maxDistance = undefined;
	L.Hash.formatHash = function(map) {
		var type = L.Hash.vehicleType;
		var sockets = L.Hash.activeSockets;
		var station = L.Hash.selectedStation;
		var maxDistance = L.Hash.maxDistance;

		var hash = [];
		// map location
		var center = map.getCenter(),
		    zoom = map.getZoom(),
		    precision = Math.max(0, Math.ceil(Math.log(zoom) / Math.LN2));
		hash.push(zoom);
		hash.push(center.lat.toFixed(precision));
		hash.push(center.lng.toFixed(precision));
		// curently active vehicle type
		type = type != "car" ? "type:" + type : type;
		type != "car" && hash.push(type);
		// active sockets
		sockets = sockets.length != 0 ? "sockets:" + sockets.join(",") : [];
		sockets.length != 0 && hash.push(sockets);
		// selected station
		station = station ? "station:" + station : undefined;
		station && hash.push(station);
		// max distance
		maxDistance = maxDistance ? "maxDistance:" + maxDistance : undefined;
		maxDistance && hash.push(maxDistance);

		return "#" + hash.join("/");
	},

	L.Hash.prototype = {
		map: null,
		lastHash: null,

		parseHash: L.Hash.parseHash,
		formatHash: L.Hash.formatHash,

		init: function(map) {
			this.map = map;

			// reset the hash
			this.lastHash = null;
			this.onHashChange(true);

			if (!this.isListening) {
				this.startListening();
			}
		},

		removeFrom: function(map) {
			if (this.changeTimeout) {
				clearTimeout(this.changeTimeout);
			}

			if (this.isListening) {
				this.stopListening();
			}

			this.map = null;
		},

		// triggered to update hash
		onChange: function(stationId) {
			if (stationId === 0) {
				L.Hash.selectedStation = undefined;
			} else if (! isNaN(stationId)) {
				L.Hash.selectedStation = stationId;
			}
			// bail if we're moving the map (updating from a hash),
			// or if the map is not yet loaded
			if (this.movingMap || !this.map._loaded) {
				return false;
			}

			L.Hash.vehicleType = StationLayer.options.type;
			L.Hash.activeSockets = [];
			Object.keys(StationLayer.sockets).forEach(function(key) {
				if (StationLayer.sockets[key]) {
					L.Hash.activeSockets.push(key);
				}
			});

			// on default-change of maxDistance the value
			// compaired to route.maxDistance has to be adjusted
			L.Hash.maxDistance = (route.maxDistance != 200000) ? route.maxDistance / 1000 : undefined;

			var hash = this.formatHash(this.map);
			if (this.lastHash != hash) {
				location.replace(hash);
				this.lastHash = hash;
			}
		},

		movingMap: false,
		update: function() {
			var hash = location.hash;
			if (hash === this.lastHash) {
				return;
			}
			var parsed = this.parseHash(hash);
			if (parsed) {
				this.movingMap = true;

				this.map.setView(parsed.center, parsed.zoom);

				this.movingMap = false;
			} else {
				this.onChange(this.map);
			}
		},

		// defer hash change updates every 100ms
		changeDefer: 100,
		changeTimeout: null,
		onHashChange: function(noTimeout) {
			// throttle calls to update() so that they only happen every
			// `changeDefer` ms
			if (!this.changeTimeout) {
				if (noTimeout) {
					this.update();
					return;
				}
				var that = this;
				this.changeTimeout = setTimeout(function() {
					that.update();
					that.changeTimeout = null;
				}, this.changeDefer);
			}
		},

		isListening: false,
		hashChangeInterval: null,
		startListening: function() {
			this.map.on("moveend", this.onChange, this);

			if (HAS_HASHCHANGE) {
				L.DomEvent.addListener(window, "hashchange", this.onHashChange);
			} else {
				clearInterval(this.hashChangeInterval);
				this.hashChangeInterval = setInterval(this.onHashChange, 50);
			}
			this.isListening = true;
		},

		stopListening: function() {
			this.map.off("moveend", this.onChange, this);

			if (HAS_HASHCHANGE) {
				L.DomEvent.removeListener(window, "hashchange", this.onHashChange);
			} else {
				clearInterval(this.hashChangeInterval);
			}
			this.isListening = false;
		}
	};
	L.hash = function(map) {
		return new L.Hash(map);
	};
	L.Map.prototype.addHash = function() {
		this._hash = L.hash(this);
	};
	L.Map.prototype.removeHash = function() {
		this._hash.removeFrom();
	};
})(window);
