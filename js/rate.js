var rate = {

  initCallbacks: 0,
  initCallbacksCounter: 0,

  rateRoutes: function () {
    rate.allStationsNearRoutes = [];
    rate.allRequestsSent = false;

    // remove double entries, so we do not waste OverpassTurbo
    // ressources and time running around requests
    var tmp = {};
    Object.keys(route.allRoutes).forEach(function(key) {
      if (route.allRoutes[key].stations !== undefined) {
        for (var k = 0; k < route.allRoutes[key].stations.length; k++) {
          var newKey = route.allRoutes[key].stations[k].lat + "-" + route.allRoutes[key].stations[k].lon;
          tmp[newKey] = route.allRoutes[key].stations[k];
        }
      }
    });
    Object.keys(tmp).forEach(function(station) {
      rate.allStationsNearRoutes.push(tmp[station]);
    });

    rate.initCallbacks ++;
    network.setupAroundRequest(rate.allStationsNearRoutes, rate.catchRateRouteCallbacks);

    Object.keys(route.allRoutes).forEach(function(key) {
      if (route.allRoutes[key].stations !== undefined) {
        for (var i = 0; i < route.allRoutes[key].stations.length; i++) {
          rate.initCallbacks ++;
          rate.getStation_detour(route.allRoutes[key].stations[i], route.allRoutes[key], rate.catchRateRouteCallbacks);
        }
      }
    });

    rate.initCallbacks ++;
    rate.allRequestsSent = true;
    rate.catchRateRouteCallbacks();
  },

  catchRateRouteCallbacks: function () {
    rate.initCallbacksCounter ++;
    // all ajax requests finished?
    if (rate.initCallbacks <= rate.initCallbacksCounter && rate.allRequestsSent) {
      rate.rateRoute();
    }
  },

  rateRoute: function () {
    // fastestRoute contains the key of the route with the least summary.totalTime
    var fastestRoute;
    Object.keys(route.allRoutes).forEach(function(key) {
      if (!fastestRoute
        || route.allRoutes[fastestRoute].summary.totalTime
          > route.allRoutes[key].summary.totalTime) {
        fastestRoute = key;
      }
    });

    Object.keys(route.allRoutes).forEach(function(key) {
      for (var i = 0; i < route.allRoutes[key].stations.length; i++) {
        var curStation = route.allRoutes[key].stations[i];
        var curRate = 1; // base value to avoid 0 rates

        curRate += rate.rateStation_fee(curStation, 0.5);
        curRate += rate.rateStation_parkingFee(curStation, 0.25);
        curRate += rate.rateStation_authentification(curStation, 0.25);
        curRate += rate.rateStation_openingHours(curStation, 0.5);
        curRate += rate.rateStation_capacity(curStation, 0.5);
        curRate += rate.rateStation_around(curStation, 0.5);
        curRate += rate.rateStation_detour(curStation, 2.5)
        curStation.rateRoute = curRate;
      }
    });

    Object.keys(route.allRoutes).forEach(function(key) {
      route.findPath(route.allRoutes[key]);
      if (route.allRoutes[key].choosenStations) {
        route.allRoutes[key].rate = 0;
        for (var i = 0; i < route.allRoutes[key].choosenStations.length; i++) {
          route.allRoutes[key].rate += route.allRoutes[key].choosenStations[i].rateRoute;
        }
        if (key != fastestRoute) {
          // every 150 seconds a route takes longer (on 100km)
          //  than the fastest one we downvote the route by 1
          route.allRoutes[key].rate += (route.allRoutes[key].summary.totalTime - route.allRoutes[fastestRoute].summary.totalTime)
                                      / route.allRoutes[fastestRoute].summary.totalDistance * 100000
                                      / 150;
        }
      }
    });

    route.chooseBestWay();
  },

  rateStationDirectly: function (stations) {
    // if we only get one station convert it into an array
    if (stations.id !== undefined) {
      stations = [ stations ];
    }

    for (var i = 0; i < stations.length; i++) {
      var curStation = stations[i];
      var curRate = 1; // base value to avoid 0 rates
      // the following weights should add up to 5
      curRate += this.rateStation_fee(curStation, 1);
      curRate += this.rateStation_parkingFee(curStation, 0.5);
      curRate += this.rateStation_authentification(curStation, 0.5);
      curRate += this.rateStation_openingHours(curStation, 1);
      curRate += this.rateStation_capacity(curStation, 1);
      curRate += this.rateStation_around(curStation, 1);
      // the access weight is not included in 5, because access
      // only downvotes ratings, never upvotes them ...
      // this way we do not give stars for nonexisting access tags
      curRate += this.rateStation_access(curStation, 1);

      if (curRate > 5) {
        curRate = 5;
      }

      stations[i].rate = curRate;
    }
  },

  rateStation_fee: function (curStation, maxWeight) {
    if (curStation.tags.fee === "no") {
      return 0;
    }
    return maxWeight;
  },

  rateStation_parkingFee: function (curStation, maxWeight) {
    if (curStation.tags["parking:fee"] === "no") {
      return 0;
    }
    return maxWeight;
  },

  rateStation_authentification: function (curStation, maxWeight) {
    if (curStation.tags["authentication:none"] === "yes") {
      return 0;
    }
    return maxWeight;
    //TODO rate easy auth methods
  },

  rateStation_openingHours: function (curStation, maxWeight) {
    if (curStation.tags["opening_hours"] === "24/7") {
      return 0;
    }
    return maxWeight;
    //TODO rate the longer the better
  },

  rateStation_capacity: function (curStation, maxWeight) {
    // TODO: match against capacity of wanted socks
    // the more the better
    // maxWeight at 8 or more capacity
    var capacity = curStation.tags.capacity;
    if (capacity = rate.isInt(capacity)) {
      if (capacity >= 8) {
        return 0;
      } else if (capacity > 0) {
        return maxWeight / capacity;
      }
    }
    return maxWeight;
  },

  rateStation_around: function (curStation, maxWeight) {
    if (curStation.aroundPOIs === undefined || curStation.aroundPOIs === null) {
      return maxWeight;
    }
    var count = curStation.aroundPOIs.length;
    if (rate.isInt(count)) {
      if (count >= 5) {
        return 0;
      } else if (count > 0) {
        return maxWeight / count;
      }
    }
    return maxWeight;
  },

  isInt: function (input) {
    if (isNaN(input)) {
      return false;
    }
    return parseFloat(input);
  },

  rateStation_detour: function (curStation, maxWeight) {
    if (curStation.detour.addTime < 120) { // max 2min
      return 0
    } else if (curStation.detour.addTime < 300) { // max 5min
      return maxWeight / 4
    } else if (curStation.detour.addTime < 420) { // max 7min
      return maxWeight / 3
    } else if (curStation.detour.addTime < 540) { // max 9min
      return maxWeight / 2
    }
    return maxWeight;
  },

  getStation_detour: function (relevantStation, way, callback) {
    for (var i = 0; i < way.waypoints.length; i++) {
      if (way.waypoints[i].nearestRouteNode !== undefined) {
        continue;
      }
      way.waypoints[i].nearestRouteNode = rate.findNearestRouteNode(way.waypoints[i].latLng, way);
    }

    var waypoints = [];

    for (var i = 0; i < way.waypoints.length; i++) {
      waypoints.push(way.waypoints[i]);
    }
    if (way.distinctMiddle) {
      var latlng = L.latLng(way.distinctMiddle.lat, way.distinctMiddle.lng);
      waypoints.push({
        latLng: latlng,
        nearestRouteNode: rate.findNearestRouteNode(latlng, way)});
    }

    var latlng = L.latLng(relevantStation.lat, relevantStation.lon);
    relevantStation.latLng = latlng;
    relevantStation.nearestRouteNode = rate.findNearestRouteNode(latlng, way);

    waypoints.push(relevantStation);

    waypoints.sort(rate.compairNearestRouteNodes);

    var query = "";
    for (var i = 0; i < waypoints.length; i++) {
      query += waypoints[i].latLng.lng + "," + waypoints[i].latLng.lat;
      if (i !== waypoints.length - 1) {
        query += ";"
      }
    }
    query += "?overview=false&alternatives=false&steps=false";
    network.makeOSRMRequest(query, function (json) {
      rate.analyzeDetour(json, relevantStation, way, callback);
    }, function (json) {
      rate.analyzeDetour(json, relevantStation, way, callback);
    });
  },

  rateStation_access: function (curStation, maxWeight) {
    var access = curStation.tags.access;
    switch (access) {
      case 'private':
        return maxWeight;
      case 'customers':
        return maxWeight / 2;
      default:
        return 0;
    }
  },

  analyzeDetour: function (json, relevantStation, way, callback) {
    var addDistance = json.routes[0].distance - way.summary.totalDistance;
    var addTime = json.routes[0].duration - way.summary.totalTime;

    relevantStation.detour = {addDistance: addDistance, addTime: addTime};

    callback();
  },

  findNearestRouteNode: function (latlng, route, left, right) {
    if (left === undefined) {
      left = 0;
    }
    if (right === undefined) {
      right = route.coordinates.length - 1;
    }

    var middle = Math.floor(left + (right - left) / 2);

    if (right == left + 1) {
      var leftMiddle = left;
      var rightMiddle = right;
    } else {
      var leftMiddle = Math.floor(left + (middle - left) / 2);
      var rightMiddle = Math.floor(middle + (right - middle) / 2);
    }

    var dLeftMiddle = latlng.distanceTo(route.coordinates[leftMiddle]);
    var dRightMiddle = latlng.distanceTo(route.coordinates[rightMiddle]);

    if (dLeftMiddle < dRightMiddle) {
      if (right == left + 1) {
        return left;
      }
      return rate.findNearestRouteNode(latlng, route, left, middle);
    } else {
      if (right == left + 1) {
        return right;
      }
      return rate.findNearestRouteNode(latlng, route, middle, right);
    }
  },

  compairNearestRouteNodes: function (a, b) {
    if (a.nearestRouteNode < b.nearestRouteNode) {
      return -1;
    }
    if (a.nearestRouteNode > b.nearestRouteNode) {
      return 1;
    }
    // a must be equal to b
    return 0;
  }

}
