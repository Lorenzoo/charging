var Locals = {

  options: {
    language: undefined
  },

  init: function () {
    var language = navigator.language;
    Object.keys(this.locals).forEach(function(local) {
      if (language.indexOf(local) !== -1) {
        Locals.options.language = local;
      }
    });
    if (Locals.options.language == undefined) {
      Locals.options.language = "en";
    }
  },

  getText: function (category, key, language) {
    if (language === undefined) {
      language = this.options.language;
    }

    var text;

    Object.keys(this.locals).forEach(function(local) {
      if (language.indexOf(local) !== -1) {
        text = Locals.locals[local][category][key];
      }
    });

    return text;
  },

  locals: {
    'en': {
      'notification-bar': {
        'noSocketSelected': 'no socket selected ...',
        'pleaseZoomIn': 'please zoom in ...',
        'noStationSequenceFound': 'no station sequence found ...'
      },
      'search-bar': {
        'search': 'Search ...',
        'my-position': 'My position',
        'custom-distance': 'custom',
        'title': 'planning trip',
        'title-geocoders': 'Waypoints',
        'text-geocoders': 'start, stop, etc.',
        'title-range': 'Range',
        'text-range': 'maximum distance between stations:',
        'start-navigation': 'start navigation',
        'route-overview': 'show overview'
      },
      'missing-data': {
        'missing-text': 'some data on this station is still missing',
        'add': '<span class="fa fa-pencil fa-lg"></span>add'
      },
      'charging-fee': {
        'unknown': 'charging fee unknown',
        'yes': 'charge with costs',
        'no': 'charge gratis'
      },
      'parking-fee': {
        'unknown': 'parking fee unknown',
        'yes': 'parking with costs',
        'no': 'parking gratis'
      },
      'auth-methods': {
        'none': 'none',
        'phone_call': 'phone call',
        'short_message': 'SMS',
        'nfc': 'NFC',
        'money_card': 'money card',
        'debit_card': 'debit card',
        'membership_card': 'membership card',
        'unknown': '(unknown method)'
      },
      'payment-methods': {
        'cash': 'cash',
        'debit_cards': 'debit cards',
        'credit_cards': 'credit cards',
        'short_message': 'SMS',
        'unknown': '(unknown method)'
      },
      'station-info': {
        'title': 'charging station'
      },
      'access': {
        'customers': 'only for customers',
        'private': 'private'
      },
      'menu-bar': {
        'title': 'menu bar',
        'satellite': 'satellite imagery',
        'traffic': 'traffic situation',
        'about': 'about',
        'feedback': 'feedback',
        'code': 'code repository'
      },
      'buttonTitle': {
        'menuBar': 'menu',
        'route': 'create route',
        'zoomIn': 'zoom in',
        'zoomOut': 'zoom out',
        'locate': 'locate me'
      }
    },

    'de': {
      'notification-bar': {
        'noSocketSelected': 'kein Steckertyp ausgewählt ...',
        'pleaseZoomIn': 'bitte näher heran zoomen ...',
        'noStationSequenceFound': 'keine Abfolge an Stationen gefunden ...'
      },
      'search-bar': {
        'search': 'Suche ...',
        'my-position': 'Meine Position',
        'custom-distance': 'manuell',
        'title': 'Routenplanung',
        'title-geocoders': 'Wegpunkt',
        'text-geocoders': 'start, stop, etc.',
        'title-range': 'Reichweite',
        'text-range': 'maximaler Abstand zwischen Stationen:',
        'start-navigation': 'Navigation starten',
        'route-overview': 'Übersicht anzeigen'
      },
      'missing-data': {
        'missing-text': 'zu dieser Station fehlen leider noch manche Angaben',
        'add': '<span class="fa fa-pencil fa-lg"></span>hinzufügen'
      },
      'charging-fee': {
        'unknown': 'Ladekosten unbekannt',
        'yes': 'Laden mit Kosten',
        'no': 'Laden ohne Kosten'
      },
      'parking-fee': {
        'unknown': 'Parkkosten unbekannt',
        'yes': 'Parken mit Kosten',
        'no': 'Parken ohne Kosten'
      },
      'auth-methods': {
        'none': 'keine',
        'phone_call': 'Telefon Anruf',
        'short_message': 'SMS',
        'nfc': 'NFC',
        'money_card': 'Geldkarte',
        'debit_card': 'EC Karte',
        'membership_card': 'Mitgliedskarte',
        'unknown': '(unbekannte Methode)'
      },
      'payment-methods': {
        'cash': 'bar',
        'debit_cards': 'EC Karte',
        'credit_cards': 'Kredit Karte',
        'short_message': 'SMS',
        'unknown': '(unbekannte Methode)'
      },
      'station-info': {
        'title': 'Ladestation'
      },
      'access': {
        'customers': 'nur für Kunden',
        'private': 'privat'
      },
      'menu-bar': {
        'title': 'Menü',
        'satellite': 'Satelitenbild',
        'traffic': 'Straßenauslastung',
        'about': 'über',
        'feedback': 'feedback',
        'code': 'Quellcode'
      },
      'buttonTitle': {
        'menuBar': 'Menü',
        'route': 'Route erstellen',
        'zoomIn': 'näher heran zoomen',
        'zoomOut': 'weiter weg zoomen',
        'locate': 'meine Position'
      }
    }
  },

  setButtonTitle: function () {
    document.getElementById('menu-bar-button').title = this.getText('buttonTitle', 'menuBar');
    document.getElementById('search-route-button').title = this.getText('buttonTitle', 'route');
    document.getElementById('zoomIn').title = this.getText('buttonTitle', 'zoomIn');
    document.getElementById('zoomOut').title = this.getText('buttonTitle', 'zoomOut');
    document.getElementById('locateMe').title = this.getText('buttonTitle', 'locate');
    document.getElementById('startMyLocation').title = this.getText('buttonTitle', 'locate');
  },

  setText: function () {
    document.querySelector('.leaflet-routing-container .title-bar .title').innerHTML = Locals.getText('search-bar', 'title');
    document.querySelector('#leaflet-routing-geocoders-title h3').innerHTML = Locals.getText('search-bar', 'title-geocoders');
    document.querySelector("#leaflet-routing-geocoders-title p").innerHTML = Locals.getText("search-bar", "text-geocoders");
    document.querySelector('#distanceMeter h3').innerHTML = Locals.getText('search-bar', 'title-range');
    document.querySelector("#distanceMeter p").innerHTML = Locals.getText("search-bar", "text-range");
    document.querySelector('#route-start p').innerHTML = Locals.getText('search-bar', 'start-navigation');
    document.querySelector('#route-overview p').innerHTML = Locals.getText('search-bar', 'route-overview');
    // distanceMeter
    document.querySelector("#distanceMeterManual p").innerHTML = Locals.getText("search-bar", "custom-distance");
    if (! L.Hash.maxDistance) {
      document.getElementById("distanceMeterMiddle-radio").checked = true;
    }
  }
};
