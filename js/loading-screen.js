var loadingScreen = {
  percentage: {},
  totalAjaxSize: 3037414,

  updatePercentage: function (e) {
    loadingScreen.percentage[e.currentTarget.responseURL] = e.loaded;
    var subtotal = 0;
    Object.keys(loadingScreen.percentage).forEach(function(key) {
      subtotal += loadingScreen.percentage[key];
    });
    var percentage = Math.round(subtotal / loadingScreen.totalAjaxSize * 100);
    document.querySelector("#loading-perentage p").innerHTML = percentage + "%";
  },

  isVisible: function (e) {
    return ! document.getElementById("loading-screen-invisible").checked;
  },

  remove: function () {
    document.getElementById("loading-screen-invisible").checked = true;
  },

  showError: function () {
    spinner1 = document.getElementById("spinner1");
    spinner2 = document.getElementById("spinner2");

    spinner1.style["animation-duration"] = "5s";
    spinner1.style["border-top-color"] = "red";
    spinner1.style["border-bottom-color"] = "red";

    spinner2.style["animation-duration"] = "10s";
    spinner2.style["border-left-color"] = "red";
    spinner2.style["border-right-color"] = "red";
  }
}
