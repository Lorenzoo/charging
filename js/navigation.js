var navigation = {
  active: false,
  nextCoordinate: 1,
  nextInstruction: 0,

  update: function () {
    this.updateNextCoordinate();
/*
    // when the users location drifts too far away
    // of the route, recalculate it
    if (d1 > locate.curPosition.accuracy / 2 + 30) {
      // calculate new route
      return;
    }
*/
    this.updateNextInstruction();
    this.moveInstructionTable();
    this.setDistanceTillNextInstruction();
  },

  updateNextCoordinate: function () {
    var coordinates  = route.allRoutes.curRoute.coordinates;

    while (this.nextCoordinate != coordinates.length - 1) {
      var e1 = L.latLng(
            coordinates[this.nextCoordinate].lat,
            coordinates[this.nextCoordinate].lng
          ).equals([
            coordinates[this.nextCoordinate - 1].lat,
            coordinates[this.nextCoordinate - 1].lng
          ]);
      if (e1) {
        this.nextCoordinate ++;
        continue;
      }

      var nextCoordinateTmp = this.nextCoordinate;
      while (nextCoordinateTmp != coordinates.length - 1) {
        var e2 = L.latLng(
              coordinates[nextCoordinateTmp].lat,
              coordinates[nextCoordinateTmp].lng
            ).equals([
              coordinates[nextCoordinateTmp + 1].lat,
              coordinates[nextCoordinateTmp + 1].lng
            ]);
        if (!e2) {
          break;
        }
        nextCoordinateTmp ++;
      }
      if (nextCoordinateTmp == coordinates.length - 1) {
        this.nextCoordinate = nextCoordinateTmp;
        break;
      }

      var d1 = L.LineUtil.pointToSegmentDistance(
        L.point(locate.curPosition.latlng.lat, locate.curPosition.latlng.lng),
        L.point(coordinates[this.nextCoordinate - 1].lat, coordinates[this.nextCoordinate - 1].lng),
        L.point(coordinates[this.nextCoordinate].lat, coordinates[this.nextCoordinate].lng));
      var d2 = L.LineUtil.pointToSegmentDistance(
        L.point(locate.curPosition.latlng.lat, locate.curPosition.latlng.lng),
        L.point(coordinates[nextCoordinateTmp].lat, coordinates[nextCoordinateTmp].lng),
        L.point(coordinates[nextCoordinateTmp + 1].lat, coordinates[nextCoordinateTmp + 1].lng));

      if (d1 < d2) {
        break;
      }
      this.nextCoordinate ++;
    }
  },

  updateNextInstruction: function () {
    var instructions = route.allRoutes.curRoute.instructions
    for (var i = 0; i < instructions.length; i++) {
      if (instructions[i].index >= this.nextCoordinate) {
        this.nextInstruction = i;
        break;
      }
    }
  },

  moveInstructionTable: function () {
    var topShifting = -3 * this.nextInstruction;
    document.querySelector(".leaflet-routing-alt table").style.top = topShifting + "em";
  },

  setDistanceTillNextInstruction: function () {
    var instructions = route.allRoutes.curRoute.instructions,
        coordinates  = route.allRoutes.curRoute.coordinates,
        distance = locate.curPosition.latlng.distanceTo([
      coordinates[this.nextCoordinate].lat, coordinates[this.nextCoordinate].lng
    ]);

    for (var i = this.nextCoordinate; i < instructions[this.nextInstruction].index - 1; i++) {
      distance += L.latLng(coordinates[i].lat, coordinates[i].lng).distanceTo([
        coordinates[i + 1].lat, coordinates[i + 1].lng
      ]);
    }

    var formatter = new L.Routing.Formatter,
        sensitivity;
    if (distance < 2000 && distance >= 1000) {
      sensitivity = -1; // 1,3km
    } else if (distance < 1000 && distance >= 400) {
      sensitivity = 1; // 600m
    } else if (distance < 400) {
      sensitivity = 2; // 350m
    } else {
      sensitivity = 0; // 5km
    }
    distance = formatter.formatDistance(distance, sensitivity);
    document.querySelectorAll(".leaflet-routing-alt table tbody tr")
      [this.nextInstruction].childNodes[1].innerHTML = distance;
  },

  reset: function () {

  },

  show: function () {
    this.active = true;
    document.getElementById("leaflet-routing-alternatives-container").style.display = "block";
    document.getElementById("leaflet-routing-alternatives-container").style["z-index"] = 3;
    navigation.update();
    locate.centerOnMarker();
  },

  hide: function () {
    this.active = false;
    document.getElementById("leaflet-routing-alternatives-container").style.display = "none";
    document.getElementById("leaflet-routing-alternatives-container").style["z-index"] = 0;
  }
}
