var leafletRoutingMachine, menuBarSwiper, stationInfoSwiper, editorSwiper;
var initialisation = {

  ajaxRequestsSent: 0,
  ajaxRequestsDone: 0,
  allAjaxRequestsSent: false,

  init: function () {
    /*
    initialize locals
    */
    Locals.init();

    var mapboxAccessToken = 'pk.eyJ1Ijoib3NtdGVzdGluZyIsImEiOiJjajB3ZTZvY2gwMGVjMndwb25nNTNwbWxwIn0.gaKos5HcsbCJ0JMj_5Nz0w';
    leafletRoutingMachine = L.Routing.control({ /*
        waypoints: [
            L.latLng(57.74, 11.94),
            L.latLng(57.6792, 11.949)
        ], */
        //createMarker: function() { return null; },
        show: true,
        routeWhileDragging: true,
        showAlternatives: true,
        router: L.Routing.mapbox(mapboxAccessToken),
        geocoder: L.Control.Geocoder.mapbox(mapboxAccessToken),
        language: Locals.options.language
    });
    var lrmHTMLElement = leafletRoutingMachine.onAdd(map);
    document.getElementById("leaflet-search").appendChild(lrmHTMLElement);
    // reset the search-route-checkbox so the searchbar will
    // always be minimized on page load
    search.init();
    // save the original placeholder "start" and replace with "search ..."
    search.setPlaceholder();

    /*
    ajax requests to download neccessary stuff like osm-data and fonts
      all these downloads count into loadingscreen's percentage
    */
    this.ajaxRequestsSent ++;
    network.getJSON(stationResources['car'].url, this, function (json, that) {
      that.ajaxRequestsDone ++;
      stationResources['car'].data = json.elements;
      that.done();
    }, initialisation.onError);

    this.ajaxRequestsSent ++;
    network.getJSON(stationResources['undefinedSock'].url, this, function (json, that) {
      that.ajaxRequestsDone ++;
      stationResources['undefinedSock'].data = json.elements;
      that.done();
    }, initialisation.onError);

    this.ajaxRequestsSent ++;
    network.getJSON(stationResources['bicycle'].url, this, function (json, that) {
      that.ajaxRequestsDone ++;
      stationResources['bicycle'].data = json.elements;
      that.done();
    }, initialisation.onError);

    this.allAjaxRequestsSent = true;

    /*
    initialize menu-bar
    */
    document.getElementById("satellite-layer-chbox").checked = false;
    document.getElementById("traffic-layer-chbox").checked = false;
    document.querySelector("#menu-bar .title-bar .title").innerHTML += Locals.getText("menu-bar", "title");
    document.querySelector("#satellite-layer-chbox + label").innerHTML += Locals.getText("menu-bar", "satellite");
    document.querySelector("#traffic-layer-chbox + label").innerHTML += Locals.getText("menu-bar", "traffic");
    document.getElementById("about-button").innerHTML += Locals.getText("menu-bar", "about");
    document.getElementById("feedback-button").innerHTML += Locals.getText("menu-bar", "feedback");
    document.getElementById("code-repo-button").innerHTML += Locals.getText("menu-bar", "code");

    /*
    map events
      anonymous functions are necessary for IE
    */
  	map.on('click', function() { removeStationInfo() });
    map.on('mousedown', function() { menuBarSwiper.slideTo(1) });
  	map.on('moveend', function() { StationLayer.refreshStationsAround() });
    map.on('mousedown', function() { locate.disableCenteringOnMarker() });
    /*
    initialize localisation
    */
    locate.init();

    /*
    initialize all swipers
    api reference: http://idangero.us/swiper/api/
    */
    menuBarSwiper = new Swiper('#menu-bar-container', {
			slidesPerView: 'auto'
			, initialSlide: 1
			, resistanceRatio: .00000000000001
			, slideToClickedSlide: true
      , onSlideChangeEnd: function () {
        if (menuBarSwiper && menuBarSwiper.activeIndex == 0) {
          // necessary when swiper is visible and uses whole screen (100%)
					document.getElementById("menu-bar").style["box-sizing"] = "content-box";
				} else {
          // hides border when slider is out of screen
          document.getElementById("menu-bar").style["box-sizing"] = "border-box";
        }
      }
      , shortSwipes: false
      , longSwipesRatio: 0.1
      , longSwipesMs: 100
		});

    stationInfoSwiper = new Swiper('#station-info-container', {
			slidesPerView: 'auto'
			, initialSlide: 0
			, resistanceRatio: .00000000000001
			, slideToClickedSlide: true
      , onSlideChangeEnd: function () {
        if (stationInfoSwiper.activeIndex == 0
          && document.getElementById("move-station-info").checked) {
					removeStationInfo();
				}
      }
      , shortSwipes: false
      , longSwipesRatio: 0.1
      , longSwipesMs: 100
		});

    editorSwiper = new Swiper('#editor-slider-container', {
			slidesPerView: 'auto'
			, initialSlide: 0
			, resistanceRatio: .00000000000001
			, slideToClickedSlide: true
      , shortSwipes: false
      , longSwipesRatio: 0.1
      , longSwipesMs: 100
		});

    /*
    set hover titles on buttons
    */
    Locals.setButtonTitle();
    Locals.setText();
  },

  onError: function () {
    loadingScreen.showError();
  },

  done: function () {
    if (! this.allAjaxRequestsSent || this.ajaxRequestsSent != this.ajaxRequestsDone) {
      return;
    }

    console.log("download done");
    StationLayer.prepairRefresh();
    StationLayer.checkForSelectedStation();

    loadingScreen.remove();
    // focus on search bar
    document.querySelector(".leaflet-routing-geocoders .leaflet-routing-geocoder input").focus();
  }
}
