var stationResources = {
  'car': {'url': './js/overpass-static/car.json', 'data': undefined},
  'bicycle': {'url': './js/overpass-static/bicycle.json', 'data': undefined},
  'truck': {'url': '/js/overpass-static/truck', 'data': undefined},
  'scooter': {'url': '/js/overpass-static/truck', 'data': undefined},
  'undefinedSock': {'url': './js/overpass-static/undefinedSock.json', 'data': undefined}
};

var StationLayer = L.FeatureGroup.extend({
  options: {
    'minZoom': 11,
    'aroundMinZoom': 16,
    'type': 'car',
    'fallbackType': 'car',
    'undefinedSockLayer': true,
    'selectedStation': undefined
  },

  allStations: {},

  sockets: {
    schuko: false,
    type1: false,
    type2: false,
    type2_combo: false,
    chademo: false,
    tesla: false,
    cee_blue: false,
    cee_red_16a: false,
    cee_red_32a: false
  },

  initialize: function (options) {
      L.Util.setOptions(this, options);
      // reset checkboxes
      document.getElementById("notification-indicator-checkbox").checked = false;
      document.getElementById("move-station-info").checked = false;
  },

  onAdd: function () {
    this._visibleMarkers = [];
    map.on('moveend', this.prepairRefresh, this);
    this.updateSocketCheckbox();
  },

  onRemove: function (map) {
      L.LayerGroup.prototype.onRemove.call(this, map);
      map.off('moveend', this.prepairRefresh, this);
  },

  prepairRefresh: function () {
    NotificationBar.update();
    if (map.getZoom() < this.options.minZoom) {
      this._removeMarker();
      return;
    }

    // fallback type
    if (stationResources[this.options.type] === undefined) {
      this.options.type = this.options.fallbackType;
    }

    //TODO: Überprüfung auf letzte BBox

    // known socks
    this._getVisiblePOIs();
  },

  _getVisiblePOIs: function () {
    this._poi = [];
    this._undefinedSockPoi = [];
    var bounds = map.getBounds();
    var type = this.options.type;

    // active socks
    for (var i = 0; i < stationResources[type].data.length; i++) {
      var e = stationResources[type].data[i];

      if (bounds._southWest.lat > e.lat || e.lat > bounds._northEast.lat
      || bounds._southWest.lng > e.lon || e.lon > bounds._northEast.lng) {
        continue;
      }

      if ( this._hasSelectedSocket(e) && ! this._isStationVisible(e) && ! this._isStationAlreadyInRoute(e)) {
        this._poi.push(e);
      }
    }

    // get around stations poi
    if (this._poi.length !== 0) {
      network.setupAroundRequest(this._poi, this._addMarker, this);
    }

    // unknown socks
    if (this.options.undefinedSockLayer === false) {
      return;
    }

    for (var i = 0; i < stationResources['undefinedSock'].data.length; i++) {
      var e = stationResources['undefinedSock'].data[i];

      if (bounds._southWest.lat > e.lat || e.lat > bounds._northEast.lat
      || bounds._southWest.lng > e.lon || e.lon > bounds._northEast.lng) {
        continue;
      }

      if ((e.tags.car !== undefined
        || e.tags.bicycle !== undefined
        || e.tags.truck !== undefined
        || e.tags.scooter !== undefined)
        && (e.tags[type] === undefined
        || e.tags[type] == 'no')) {
        continue;
      }

      if (! this._isStationVisible(e)) {
        this._undefinedSockPoi.push(e);
      }
    }
    this._addMarker(this._undefinedSockPoi, this);
  },

  updateVehicleType: function (type) {
    switch (type) {
      case "bicycle":
        this.options.type = "bicycle";
        document.getElementById("vehicle-bicycle-chbox").checked = true;
        break;
      case "truck":
        this.options.type = "truck";
        document.getElementById("vehicle-truck-chbox").checked = true;
        break;
      case "scooter":
        this.options.type = "scooter";
        document.getElementById("vehicle-scooter-chbox").checked = true;
        break;
      default:
        this.options.type = "car";
        document.getElementById("vehicle-car-chbox").checked = true;
    }
    this._removeMarker();
    this.prepairRefresh();
    urlHash.onChange();
  },

  _hasSelectedSocket: function (e) {
    for (var sock in this.sockets) {
      if (this.sockets.hasOwnProperty(sock)
      && this.sockets[sock] === true
      && (e.tags['socket:' + sock] !== undefined
      && e.tags['socket:' + sock] != 0
      || sock === "tesla"
      && (e.tags['socket:tesla_standard'] !== undefined
      && e.tags['socket:tesla_standard'] != 0
      || e.tags['socket:tesla_supercharger'] !== undefined
      && e.tags['socket:tesla_supercharger'] != 0
      || e.tags['operator'] !== undefined
      && e.tags['operator'].indexOf('Tesla') !== -1))) {
        return true;
      }
    }
    return false;
  },

  _isStationVisible: function (e) {
    for (var i = 0; i < this._visibleMarkers.length; i++) {
      if (e.id == this._visibleMarkers[i].id) {
        return true;
      }
    }
    return false;
  },

  _isStationAlreadyInRoute: function (e) {
    if (! route.allRoutes) {
      return false;
    }
    // do not add a marker if routing has already created one
    var result = false;
    Object.keys(route.allRoutes).forEach(function(key) {
      if (route.allRoutes[key].stations !== undefined) {
        for (var i = 0; i < route.allRoutes[key].stations.length; i++) {
          if (route.allRoutes[key].stations[i].id == e.id) {
            result = true;
          }
        }
      }
    });
    return result;
  },

  _addMarker: function (relevantStations, that) {
    if (relevantStations === undefined || relevantStations.length == 0) {
      return;
    }
    // rate stations
    for (var i = 0; i < relevantStations.length; i++) {
      if (relevantStations[i].aroundPOIs !== undefined) {
        var r =  Object.create(rate);
        r.rateStationDirectly(relevantStations);
        break;
      }
    }

    for (var i = 0; i < relevantStations.length; i++) {
      marker = L.marker(
        new L.LatLng(relevantStations[i].lat, relevantStations[i].lon),
        { icon: that._getMarkerIcon(relevantStations[i]) }
      ).on('mousedown', addStationInfo);
      marker.tags = relevantStations[i].tags;
      marker.id = relevantStations[i].id;
      marker.rate = relevantStations[i].rate;
      marker.aroundPOIs = relevantStations[i].aroundPOIs;

      // currently only webkit based browsers seem to be able
      // to handle these massive bouncy-marker animations
      if (L.Browser.webkit && !L.Browser.mobileWebkit) {
        setTimeout(function(marker) {
          that._map.addLayer(marker);
          that._visibleMarkers.push(marker);
        }, Math.random() * 700, marker);
      } else {
        that._map.addLayer(marker);
        that._visibleMarkers.push(marker);
      }
    }
  },

  _removeMarker: function (onlyKnownSocks, poi) {
    if (poi) {
      // remove specific marker
      var tmp = [];
      var length = this._visibleMarkers.length;
      for (var i = 0; i < length; i++) {
        marker = this._visibleMarkers.pop();
        if (marker.id == poi.id) {
          map.removeLayer(marker);
        } else {
          tmp.push(marker);
        }
      }
      this._visibleMarkers = tmp;
    } else {
      // remove all markers
      if (! this._visibleMarkers) {
        return;
      }
      var visibleMarkersToKeep = [];
      while ((marker = this._visibleMarkers.pop()) != null) {
        if (!onlyKnownSocks || marker.rate) {
          map.removeLayer(marker);
        } else {
          visibleMarkersToKeep.push(marker);
        }
      }
      this._visibleMarkers = visibleMarkersToKeep;
    }
  },

  _getMarkerIcon: function (poi) {
    var url, size, anchor, cssClass, rate;
    rate = poi.rate ? poi.rate : poi.rateRoute;
    if (rate === undefined) {
      url = 'svg/pointers/pointer_unknown.svg';
      size = [20, 30];
      anchor = [10, 30];
      cssClass = 'unknown-marker';
    } else if (rate < 1.6) {
      url = 'svg/pointers/pointer.svg';
      size = [47, 70];
      anchor = [20, 70];
      cssClass = 'known-marker';
    } else if (rate < 2.6) {
      url = 'svg/pointers/pointer1.svg';
      size = [40, 60];
      anchor = [20, 60];
      cssClass = 'known-marker1';
    } else if (rate < 3.6) {
      url = 'svg/pointers/pointer2.svg';
      size = [37, 55];
      anchor = [17, 50];
      cssClass = 'known-marker2';
    } else if (rate < 4.6) {
      url = 'svg/pointers/pointer3.svg';
      size = [30, 45];
      anchor = [13.5, 40];
      cssClass = 'known-marker3';
    } else {
      url = 'svg/pointers/pointer4.svg';
      size = [23, 35];
      anchor = [10, 30];
      cssClass = 'known-marker4';
    }

    var options = {
      iconUrl: url,
      shadowUrl: '',
      iconSize: size,
      iconAnchor: anchor,
      className: cssClass,
      html: '<object class="station-marker-divIcon" type="image/svg+xml" data="' + url + '"></object>'
    }
    // currently only webkit based browsers seem to be able
    // to handle these massive bouncy-marker animations
    if (L.Browser.webkit && !L.Browser.mobileWebkit) {
      icon = L.divIcon(options);
    } else {
      icon = L.icon(options);
    }

    return icon;
  },

  changeSocket: function (socket, disable) {
    // socket name is not listed
    if (!(socket in this.sockets)) {
      console.log("net drinn");
      return false;
    }

    var tmpQuery = '';

    if (this.sockets[socket] == false && ! disable) {
      // activate socket
      this.sockets[socket] = true;
      /*
        activating checkboxes by javascript, because labels in Firefox
        do not work as I'd like them to
        if the mouse/pointer is moving while clicking on the label
        Firefox does not activate the checkbox

        each socket needs a checkbox with an id like
        "[socketname]-checkbox"
      */
      document.getElementById(socket + "-checkbox").checked = true;
    } else {
      // deactivate socket
      this.sockets[socket] = false;
      document.getElementById(socket + "-checkbox").checked = false;
    }

    this._removeMarker(true); //TODO nur die nicht mehr gebrauchten Marker entfernen
    this.prepairRefresh();
    leafletRoutingMachine.route();
    urlHash.onChange();
  },

  _isOneSocketEnabled: function () {
    var oneEnabled = false;
    for (var sock in this.sockets) {
      if (this.sockets[sock] == true) {
        oneEnabled = true;
        break;
      }
    }
    return oneEnabled;
  },

  updateSocketCheckbox: function () {
    var socketsBoxes = document.querySelectorAll(".socket-chbox");
    for (var i = 0; i < socketsBoxes.length; i++) {
      var socket = socketsBoxes[i].id.replace("-checkbox", "");
      if (this.sockets[socket]) {
        socketsBoxes[i].checked = true;
      } else {
        socketsBoxes[i].checked = false;
      }
    }
  },

  checkForSelectedStation: function () {
    var stationId = this.options.selectedStation;

    if (!stationId) {
      return;
    }

    for (var i = 0; i < stationResources[this.options.type].data.length; i++) {
      if (stationResources[this.options.type].data[i].id == this.options.selectedStation) {
        addStationInfo({
          "target": stationResources[this.options.type].data[i],
          "containerPoint": {"x": 0}
        });
        return;
      }
    }

    for (var i = 0; i < stationResources["undefinedSock"].data.length; i++) {
      if (stationResources["undefinedSock"].data[i].id == this.options.selectedStation) {
        addStationInfo({
          "target": stationResources["undefinedSock"].data[i],
          "containerPoint": {"x": 0}
        });
        return;
      }
    }
  },

  refreshStationsAround: function () {
    return;
    if (this._visibleAroundMarkers === undefined) {
      this._visibleAroundMarkers = [];
    }

    if (map.getZoom() < this.options.aroundMinZoom || ! this._isOneSocketEnabled()) {
      this._removeStationsAround();
      return;
    }

    var type = this.options.type;
    var bounds = map.getBounds();
    for (var i = 0; i < stationResources[type].data.length; i++) {
      var e = stationResources[type].data[i];
      if (bounds._southWest.lat > e.lat || e.lat > bounds._northEast.lat
      || bounds._southWest.lng > e.lon || e.lon > bounds._northEast.lng
      || !stationResources[type].data[i].aroundPOIs) {
        continue;
      }

      for (var k = 0; k < stationResources[type].data[i].aroundPOIs.length; k++) {
        var a = stationResources[type].data[i].aroundPOIs[k];
        if (!this._isStationsAroundVisible(a)) {
          this._addStationsAround(a);
        }
      }
    }
  },

  _removeStationsAround: function () {
    for (var i = 0; i < this._visibleAroundMarkers.length; i++) {
      map.removeLayer(this._visibleAroundMarkers[i]);
    }
  },

  _addStationsAround: function (e) {
    var marker = L.marker(
      [e.lat, e.lon],
      { icon: L.divIcon({
        iconSize: [150,20],
        iconAnchor: [10,10],
        className: "",
        html: '<div class="aroundPOI">' + e.tags.name + '</div>'
      }) }
    ).addTo(map);
    this._visibleAroundMarkers.push(marker);
  },

  _isStationsAroundVisible: function (e) {
    for (var i = 0; i < this._visibleAroundMarkers.length; i++) {
      if (e.id == this._visibleAroundMarkers[i].id) {
        return true;
      }
    }
    return false;
  }
});

L.StationLayer = StationLayer;
