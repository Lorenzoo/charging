# Chagre'em up

Webapp to find best suited charging station on your way and destination. Makes use of OpenStreetMap.

**Running instance [»here«](https://Lorenzoo.gitlab.io/charging/).**

## Description
This webapp tries to help in finding charging stations for cars, bicycles, scooters and trucks. Maps like OpenChargeMap etc. display a huge bulk of stations in cities like Berlin, but as a driver there's no way in comparing all of them. There needs to be a system doing some  pre-voting for you. That's exactly what this project aims to do. [Rating](#rating) and suggesting stations.

## OpenStreetMap
Many other maps run their own data-store of locations. Most of them are closed and proprietary. But of course you as a user should help improve them by entering new positions...
Here only data form the OpenStreetMap community, licensed under the [ODbL](https://www.openstreetmap.org/copyright), will be used.

## JavaScript
These days most websites request a load of crap they don't actually need to provide their service. Tracking users and digging into their privacy sadly has become "normal". Also minimized scripts have become a "thing". How should anyone review the code?
Hence this project only uses [free (libre)](https://www.gnu.org/philosophy/free-sw.en.html) JavaScript necessary to do the job. No tracking scripts like GoogleAnalytics etc.

## Screenshots
![screenshot of map][amt_eiderstedt]
![screenshot of map][munich]
![screenshot of map][route_aurach]
![screenshot of map][satellite_stuttgart]
<p>© <a href='https://www.mapbox.com/about/maps/' target='_blank'>Mapbox</a> © <a href='https://www.openstreetmap.org/copyright' target='_blank'>OpenStreetMap</a> contributors <strong><a href='https://www.mapbox.com/map-feedback/' target='_blank'>Improve this map</a></strong></p>

[amt_eiderstedt]: images/screenshots/amt_eidersted.png
[munich]: images/screenshots/munich.png
[route_aurach]: images/screenshots/route_aurach.png
[satellite_stuttgart]: images/screenshots/satellite_stuttgart.png

## Rating
What concrete attributes are consider in evaluating a station? Among others the fee (charging and parking), authentication methods, opening hours, capacity (how many sockets are there, how many can be used simultaneously), what's around the station (toilets, restaurants, etc.), how long would the detour be, etc.

Currently every feature has a fixed weight. This should change in the future... The algorithm should be balanced by the users preferences.

## Currentness of OSM data
A prior version directly requested charging stations via the Overpass API. This way the data almost was "live". Newly added stations would be visible only one minute later. But sadly this mechanism was unusable, because of the time each Overpass API request took. Also Overpass API blocks users if they make too much requests within a given period.
Currently on pageload a "subset" of the OSM database will be downloaded and used locally. This subset has to be extracted regularly via OverpassTurbo. Every now and then they will be updated. The Overpass API is currently only used for around requests (toilets, restaurants, etc.), downloading this info would take too long when loading the page.

Current status:

| vehicle type | count    | last updated  | size |
|--------------|----------|---------------|------|
| car          | 3377     | 25.05.2017    | 1,2MB |
| bicycle      | 376      | 25.05.2017    | 0,14MB |
| scooter      | ???      | ??.??.????    | ??MB |
| truck        | ???      | ??.??.????    | ??MB |
| unknown sock | ???      | 13.03.2017    | 1,8MB |

## Issues
Feel free to create new issues whether to report a bug or request a feature. If you do not want to create an account on GitLab and do not have any other you could log into with, just send an email to <a href="mailto:incoming+Lorenzoo/charging@gitlab.com">incoming+Lorenzoo/charging@gitlab.com</a> and a new issue will automatically created.
